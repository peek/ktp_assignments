The Makefile in the 'code' directory compiles the compiler.
The default target produces class files in the 'build' directory,
where the class 'UcParse' is the starting point.
The 'jar' target produces a runnable jar file.
For further documentation of the Makefile see the Makefile.

The output for the requested test cases can be found in the 'tests' directory.
Files with the extension '.s' is assembly code generated from corresponding
files in the test suite. Files ending with '.txt' is the generated output
when running the corresponding '.s' file in the SPIM simulator. (Note that a
few lines produced by the SPIM simulator is included in the '.txt' files.)

The file 'fac.s' has been run with the following line as input:
5

The file 'sim10.s' has been run with the following two lines as input:
Merlin
456

The input to 'fac.s' and 'sim10.s' is not visible in
corresponding output files.
