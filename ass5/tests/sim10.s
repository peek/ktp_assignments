	.data
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -628
	addiu	$t0, $0, 8
	sw	$t0, -120($fp)
	lw	$t3, -120($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -120($fp)
	addiu	$t0, $0, 20
	sw	$t0, -124($fp)
	lw	$t3, -124($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -124($fp)
	addiu	$t0, $0, 32
	sw	$t0, -128($fp)
	lw	$t3, -128($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -128($fp)
	addiu	$t0, $0, 36
	sw	$t0, -132($fp)
	lw	$t3, -132($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -132($fp)
	addiu	$t0, $0, 116
	sw	$t0, -136($fp)
	lw	$t3, -136($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -136($fp)
	addiu	$t0, $0, 0
	sw	$t0, -144($fp)
	lw	$t2, -120($fp)
	lw	$t3, -144($fp)
	add	$t0, $t2, $t3
	sw	$t0, -148($fp)
	addiu	$t0, $0, 89
	sw	$t0, -152($fp)
	lw	$t0, -152($fp)
	lw	$t1, -148($fp)
	sb	$t0, ($t1)
	lw	$t0, -148($fp)
	lb	$t1, ($t0)
	sw	$t1, -152($fp)
	addiu	$t0, $0, 1
	sw	$t0, -156($fp)
	lw	$t2, -120($fp)
	lw	$t3, -156($fp)
	add	$t0, $t2, $t3
	sw	$t0, -160($fp)
	addiu	$t0, $0, 111
	sw	$t0, -164($fp)
	lw	$t0, -164($fp)
	lw	$t1, -160($fp)
	sb	$t0, ($t1)
	lw	$t0, -160($fp)
	lb	$t1, ($t0)
	sw	$t1, -164($fp)
	addiu	$t0, $0, 2
	sw	$t0, -168($fp)
	lw	$t2, -120($fp)
	lw	$t3, -168($fp)
	add	$t0, $t2, $t3
	sw	$t0, -172($fp)
	addiu	$t0, $0, 117
	sw	$t0, -176($fp)
	lw	$t0, -176($fp)
	lw	$t1, -172($fp)
	sb	$t0, ($t1)
	lw	$t0, -172($fp)
	lb	$t1, ($t0)
	sw	$t1, -176($fp)
	addiu	$t0, $0, 3
	sw	$t0, -180($fp)
	lw	$t2, -120($fp)
	lw	$t3, -180($fp)
	add	$t0, $t2, $t3
	sw	$t0, -184($fp)
	addiu	$t0, $0, 114
	sw	$t0, -188($fp)
	lw	$t0, -188($fp)
	lw	$t1, -184($fp)
	sb	$t0, ($t1)
	lw	$t0, -184($fp)
	lb	$t1, ($t0)
	sw	$t1, -188($fp)
	addiu	$t0, $0, 4
	sw	$t0, -192($fp)
	lw	$t2, -120($fp)
	lw	$t3, -192($fp)
	add	$t0, $t2, $t3
	sw	$t0, -196($fp)
	addiu	$t0, $0, 32
	sw	$t0, -200($fp)
	lw	$t0, -200($fp)
	lw	$t1, -196($fp)
	sb	$t0, ($t1)
	lw	$t0, -196($fp)
	lb	$t1, ($t0)
	sw	$t1, -200($fp)
	addiu	$t0, $0, 5
	sw	$t0, -204($fp)
	lw	$t2, -120($fp)
	lw	$t3, -204($fp)
	add	$t0, $t2, $t3
	sw	$t0, -208($fp)
	addiu	$t0, $0, 110
	sw	$t0, -212($fp)
	lw	$t0, -212($fp)
	lw	$t1, -208($fp)
	sb	$t0, ($t1)
	lw	$t0, -208($fp)
	lb	$t1, ($t0)
	sw	$t1, -212($fp)
	addiu	$t0, $0, 6
	sw	$t0, -216($fp)
	lw	$t2, -120($fp)
	lw	$t3, -216($fp)
	add	$t0, $t2, $t3
	sw	$t0, -220($fp)
	addiu	$t0, $0, 97
	sw	$t0, -224($fp)
	lw	$t0, -224($fp)
	lw	$t1, -220($fp)
	sb	$t0, ($t1)
	lw	$t0, -220($fp)
	lb	$t1, ($t0)
	sw	$t1, -224($fp)
	addiu	$t0, $0, 7
	sw	$t0, -228($fp)
	lw	$t2, -120($fp)
	lw	$t3, -228($fp)
	add	$t0, $t2, $t3
	sw	$t0, -232($fp)
	addiu	$t0, $0, 109
	sw	$t0, -236($fp)
	lw	$t0, -236($fp)
	lw	$t1, -232($fp)
	sb	$t0, ($t1)
	lw	$t0, -232($fp)
	lb	$t1, ($t0)
	sw	$t1, -236($fp)
	addiu	$t0, $0, 8
	sw	$t0, -240($fp)
	lw	$t2, -120($fp)
	lw	$t3, -240($fp)
	add	$t0, $t2, $t3
	sw	$t0, -244($fp)
	addiu	$t0, $0, 101
	sw	$t0, -248($fp)
	lw	$t0, -248($fp)
	lw	$t1, -244($fp)
	sb	$t0, ($t1)
	lw	$t0, -244($fp)
	lb	$t1, ($t0)
	sw	$t1, -248($fp)
	addiu	$t0, $0, 9
	sw	$t0, -252($fp)
	lw	$t2, -120($fp)
	lw	$t3, -252($fp)
	add	$t0, $t2, $t3
	sw	$t0, -256($fp)
	addiu	$t0, $0, 63
	sw	$t0, -260($fp)
	lw	$t0, -260($fp)
	lw	$t1, -256($fp)
	sb	$t0, ($t1)
	lw	$t0, -256($fp)
	lb	$t1, ($t0)
	sw	$t1, -260($fp)
	addiu	$t0, $0, 10
	sw	$t0, -264($fp)
	lw	$t2, -120($fp)
	lw	$t3, -264($fp)
	add	$t0, $t2, $t3
	sw	$t0, -268($fp)
	addiu	$t0, $0, 32
	sw	$t0, -272($fp)
	lw	$t0, -272($fp)
	lw	$t1, -268($fp)
	sb	$t0, ($t1)
	lw	$t0, -268($fp)
	lb	$t1, ($t0)
	sw	$t1, -272($fp)
	addiu	$t0, $0, 11
	sw	$t0, -276($fp)
	lw	$t2, -120($fp)
	lw	$t3, -276($fp)
	add	$t0, $t2, $t3
	sw	$t0, -280($fp)
	addiu	$t0, $0, 0
	sw	$t0, -284($fp)
	lw	$t0, -284($fp)
	lw	$t1, -280($fp)
	sb	$t0, ($t1)
	lw	$t0, -280($fp)
	lb	$t1, ($t0)
	sw	$t1, -284($fp)
	addiu	$t0, $0, 0
	sw	$t0, -288($fp)
	lw	$t2, -124($fp)
	lw	$t3, -288($fp)
	add	$t0, $t2, $t3
	sw	$t0, -292($fp)
	addiu	$t0, $0, 89
	sw	$t0, -296($fp)
	lw	$t0, -296($fp)
	lw	$t1, -292($fp)
	sb	$t0, ($t1)
	lw	$t0, -292($fp)
	lb	$t1, ($t0)
	sw	$t1, -296($fp)
	addiu	$t0, $0, 1
	sw	$t0, -300($fp)
	lw	$t2, -124($fp)
	lw	$t3, -300($fp)
	add	$t0, $t2, $t3
	sw	$t0, -304($fp)
	addiu	$t0, $0, 111
	sw	$t0, -308($fp)
	lw	$t0, -308($fp)
	lw	$t1, -304($fp)
	sb	$t0, ($t1)
	lw	$t0, -304($fp)
	lb	$t1, ($t0)
	sw	$t1, -308($fp)
	addiu	$t0, $0, 2
	sw	$t0, -312($fp)
	lw	$t2, -124($fp)
	lw	$t3, -312($fp)
	add	$t0, $t2, $t3
	sw	$t0, -316($fp)
	addiu	$t0, $0, 117
	sw	$t0, -320($fp)
	lw	$t0, -320($fp)
	lw	$t1, -316($fp)
	sb	$t0, ($t1)
	lw	$t0, -316($fp)
	lb	$t1, ($t0)
	sw	$t1, -320($fp)
	addiu	$t0, $0, 3
	sw	$t0, -324($fp)
	lw	$t2, -124($fp)
	lw	$t3, -324($fp)
	add	$t0, $t2, $t3
	sw	$t0, -328($fp)
	addiu	$t0, $0, 114
	sw	$t0, -332($fp)
	lw	$t0, -332($fp)
	lw	$t1, -328($fp)
	sb	$t0, ($t1)
	lw	$t0, -328($fp)
	lb	$t1, ($t0)
	sw	$t1, -332($fp)
	addiu	$t0, $0, 4
	sw	$t0, -336($fp)
	lw	$t2, -124($fp)
	lw	$t3, -336($fp)
	add	$t0, $t2, $t3
	sw	$t0, -340($fp)
	addiu	$t0, $0, 32
	sw	$t0, -344($fp)
	lw	$t0, -344($fp)
	lw	$t1, -340($fp)
	sb	$t0, ($t1)
	lw	$t0, -340($fp)
	lb	$t1, ($t0)
	sw	$t1, -344($fp)
	addiu	$t0, $0, 5
	sw	$t0, -348($fp)
	lw	$t2, -124($fp)
	lw	$t3, -348($fp)
	add	$t0, $t2, $t3
	sw	$t0, -352($fp)
	addiu	$t0, $0, 97
	sw	$t0, -356($fp)
	lw	$t0, -356($fp)
	lw	$t1, -352($fp)
	sb	$t0, ($t1)
	lw	$t0, -352($fp)
	lb	$t1, ($t0)
	sw	$t1, -356($fp)
	addiu	$t0, $0, 6
	sw	$t0, -360($fp)
	lw	$t2, -124($fp)
	lw	$t3, -360($fp)
	add	$t0, $t2, $t3
	sw	$t0, -364($fp)
	addiu	$t0, $0, 103
	sw	$t0, -368($fp)
	lw	$t0, -368($fp)
	lw	$t1, -364($fp)
	sb	$t0, ($t1)
	lw	$t0, -364($fp)
	lb	$t1, ($t0)
	sw	$t1, -368($fp)
	addiu	$t0, $0, 7
	sw	$t0, -372($fp)
	lw	$t2, -124($fp)
	lw	$t3, -372($fp)
	add	$t0, $t2, $t3
	sw	$t0, -376($fp)
	addiu	$t0, $0, 101
	sw	$t0, -380($fp)
	lw	$t0, -380($fp)
	lw	$t1, -376($fp)
	sb	$t0, ($t1)
	lw	$t0, -376($fp)
	lb	$t1, ($t0)
	sw	$t1, -380($fp)
	addiu	$t0, $0, 8
	sw	$t0, -384($fp)
	lw	$t2, -124($fp)
	lw	$t3, -384($fp)
	add	$t0, $t2, $t3
	sw	$t0, -388($fp)
	addiu	$t0, $0, 32
	sw	$t0, -392($fp)
	lw	$t0, -392($fp)
	lw	$t1, -388($fp)
	sb	$t0, ($t1)
	lw	$t0, -388($fp)
	lb	$t1, ($t0)
	sw	$t1, -392($fp)
	addiu	$t0, $0, 9
	sw	$t0, -396($fp)
	lw	$t2, -124($fp)
	lw	$t3, -396($fp)
	add	$t0, $t2, $t3
	sw	$t0, -400($fp)
	addiu	$t0, $0, 0
	sw	$t0, -404($fp)
	lw	$t0, -404($fp)
	lw	$t1, -400($fp)
	sb	$t0, ($t1)
	lw	$t0, -400($fp)
	lb	$t1, ($t0)
	sw	$t1, -404($fp)
	addiu	$t0, $0, 0
	sw	$t0, -408($fp)
	lw	$t2, -128($fp)
	lw	$t3, -408($fp)
	add	$t0, $t2, $t3
	sw	$t0, -412($fp)
	addiu	$t0, $0, 89
	sw	$t0, -416($fp)
	lw	$t0, -416($fp)
	lw	$t1, -412($fp)
	sb	$t0, ($t1)
	lw	$t0, -412($fp)
	lb	$t1, ($t0)
	sw	$t1, -416($fp)
	addiu	$t0, $0, 1
	sw	$t0, -420($fp)
	lw	$t2, -128($fp)
	lw	$t3, -420($fp)
	add	$t0, $t2, $t3
	sw	$t0, -424($fp)
	addiu	$t0, $0, 111
	sw	$t0, -428($fp)
	lw	$t0, -428($fp)
	lw	$t1, -424($fp)
	sb	$t0, ($t1)
	lw	$t0, -424($fp)
	lb	$t1, ($t0)
	sw	$t1, -428($fp)
	addiu	$t0, $0, 2
	sw	$t0, -432($fp)
	lw	$t2, -128($fp)
	lw	$t3, -432($fp)
	add	$t0, $t2, $t3
	sw	$t0, -436($fp)
	addiu	$t0, $0, 117
	sw	$t0, -440($fp)
	lw	$t0, -440($fp)
	lw	$t1, -436($fp)
	sb	$t0, ($t1)
	lw	$t0, -436($fp)
	lb	$t1, ($t0)
	sw	$t1, -440($fp)
	addiu	$t0, $0, 3
	sw	$t0, -444($fp)
	lw	$t2, -128($fp)
	lw	$t3, -444($fp)
	add	$t0, $t2, $t3
	sw	$t0, -448($fp)
	addiu	$t0, $0, 32
	sw	$t0, -452($fp)
	lw	$t0, -452($fp)
	lw	$t1, -448($fp)
	sb	$t0, ($t1)
	lw	$t0, -448($fp)
	lb	$t1, ($t0)
	sw	$t1, -452($fp)
	addiu	$t0, $0, 4
	sw	$t0, -456($fp)
	lw	$t2, -128($fp)
	lw	$t3, -456($fp)
	add	$t0, $t2, $t3
	sw	$t0, -460($fp)
	addiu	$t0, $0, 97
	sw	$t0, -464($fp)
	lw	$t0, -464($fp)
	lw	$t1, -460($fp)
	sb	$t0, ($t1)
	lw	$t0, -460($fp)
	lb	$t1, ($t0)
	sw	$t1, -464($fp)
	addiu	$t0, $0, 5
	sw	$t0, -468($fp)
	lw	$t2, -128($fp)
	lw	$t3, -468($fp)
	add	$t0, $t2, $t3
	sw	$t0, -472($fp)
	addiu	$t0, $0, 114
	sw	$t0, -476($fp)
	lw	$t0, -476($fp)
	lw	$t1, -472($fp)
	sb	$t0, ($t1)
	lw	$t0, -472($fp)
	lb	$t1, ($t0)
	sw	$t1, -476($fp)
	addiu	$t0, $0, 6
	sw	$t0, -480($fp)
	lw	$t2, -128($fp)
	lw	$t3, -480($fp)
	add	$t0, $t2, $t3
	sw	$t0, -484($fp)
	addiu	$t0, $0, 101
	sw	$t0, -488($fp)
	lw	$t0, -488($fp)
	lw	$t1, -484($fp)
	sb	$t0, ($t1)
	lw	$t0, -484($fp)
	lb	$t1, ($t0)
	sw	$t1, -488($fp)
	addiu	$t0, $0, 7
	sw	$t0, -492($fp)
	lw	$t2, -128($fp)
	lw	$t3, -492($fp)
	add	$t0, $t2, $t3
	sw	$t0, -496($fp)
	addiu	$t0, $0, 58
	sw	$t0, -500($fp)
	lw	$t0, -500($fp)
	lw	$t1, -496($fp)
	sb	$t0, ($t1)
	lw	$t0, -496($fp)
	lb	$t1, ($t0)
	sw	$t1, -500($fp)
	addiu	$t0, $0, 8
	sw	$t0, -504($fp)
	lw	$t2, -128($fp)
	lw	$t3, -504($fp)
	add	$t0, $t2, $t3
	sw	$t0, -508($fp)
	addiu	$t0, $0, 32
	sw	$t0, -512($fp)
	lw	$t0, -512($fp)
	lw	$t1, -508($fp)
	sb	$t0, ($t1)
	lw	$t0, -508($fp)
	lb	$t1, ($t0)
	sw	$t1, -512($fp)
	addiu	$t0, $0, 9
	sw	$t0, -516($fp)
	lw	$t2, -128($fp)
	lw	$t3, -516($fp)
	add	$t0, $t2, $t3
	sw	$t0, -520($fp)
	addiu	$t0, $0, 0
	sw	$t0, -524($fp)
	lw	$t0, -524($fp)
	lw	$t1, -520($fp)
	sb	$t0, ($t1)
	lw	$t0, -520($fp)
	lb	$t1, ($t0)
	sw	$t1, -524($fp)
	addiu	$t0, $0, 0
	sw	$t0, -528($fp)
	lw	$t2, -132($fp)
	lw	$t3, -528($fp)
	add	$t0, $t2, $t3
	sw	$t0, -532($fp)
	addiu	$t0, $0, 10
	sw	$t0, -536($fp)
	lw	$t0, -536($fp)
	lw	$t1, -532($fp)
	sb	$t0, ($t1)
	lw	$t0, -532($fp)
	lb	$t1, ($t0)
	sw	$t1, -536($fp)
	addiu	$t0, $0, 1
	sw	$t0, -540($fp)
	lw	$t2, -132($fp)
	lw	$t3, -540($fp)
	add	$t0, $t2, $t3
	sw	$t0, -544($fp)
	addiu	$t0, $0, 0
	sw	$t0, -548($fp)
	lw	$t0, -548($fp)
	lw	$t1, -544($fp)
	sb	$t0, ($t1)
	lw	$t0, -544($fp)
	lb	$t1, ($t0)
	sw	$t1, -548($fp)
	lw	$t0, -120($fp)
	sw	$t0, -552($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -552($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -556($fp)
	lw	$t0, -136($fp)
	sw	$t0, -560($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -560($fp)
	sw	$t0, 4($sp)
	jal	getstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -564($fp)
	lw	$t0, -124($fp)
	sw	$t0, -568($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -568($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -572($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -12
	jal	getintF
	addi	$sp, $sp, 12
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -576($fp)
	lw	$t0, -576($fp)
	sw	$t0, -140($fp)
	lw	$t0, -128($fp)
	sw	$t0, -580($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -580($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -584($fp)
	lw	$t0, -136($fp)
	sw	$t0, -588($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -588($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -592($fp)
	lw	$t0, -132($fp)
	sw	$t0, -596($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -596($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -600($fp)
	lw	$t0, -128($fp)
	sw	$t0, -604($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -604($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -608($fp)
	lw	$t0, -140($fp)
	sw	$t0, -612($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -612($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -616($fp)
	lw	$t0, -132($fp)
	sw	$t0, -620($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -620($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -624($fp)
mainFend:
	addi	$sp, $sp, 628
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
