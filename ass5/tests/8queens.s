	.data
nG:	.space	4
boardG:	.space	32
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
printboardF:
	move	$fp, $sp
	addi	$sp, $sp, -52
	addiu	$t0, $0, 0
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	sw	$t0, 0($fp)
loop0:
	lw	$t0, 0($fp)
	sw	$t0, -8($fp)
	la	$t0, nG
	sw	$t0, -12($fp)
	lw	$t0, -12($fp)
	lw	$t1, ($t0)
	sw	$t1, -12($fp)
	lw	$t2, -8($fp)
	lw	$t3, -12($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -16($fp)
	lw	$t0, -16($fp)
	beq	$t0, $0, endLoop0
	lw	$t0, 0($fp)
	sw	$t0, -20($fp)
	addiu	$t0, $0, 4
	sw	$t0, -24($fp)
	lw	$t2, -24($fp)
	lw	$t3, -20($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -20($fp)
	lw	$t2, -20($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -28($fp)
	lw	$t0, -28($fp)
	lw	$t1, ($t0)
	sw	$t1, -32($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -32($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -36($fp)
	lw	$t0, 0($fp)
	sw	$t0, -40($fp)
	addiu	$t0, $0, 1
	sw	$t0, -44($fp)
	lw	$t2, -40($fp)
	lw	$t3, -44($fp)
	add	$t0, $t2, $t3
	sw	$t0, -48($fp)
	lw	$t0, -48($fp)
	sw	$t0, 0($fp)
	j	loop0
endLoop0:
printboardFend:
	addi	$sp, $sp, 52
	jr	$ra
checkF:	move	$fp, $sp
	addi	$sp, $sp, -164
	lw	$t0, 4($fp)
	sw	$t0, -8($fp)
	addiu	$t0, $0, 1
	sw	$t0, -12($fp)
	lw	$t2, -8($fp)
	lw	$t3, -12($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -16($fp)
	lw	$t0, -16($fp)
	sw	$t0, 0($fp)
loop1:
	lw	$t0, 0($fp)
	sw	$t0, -20($fp)
	addiu	$t0, $0, 0
	sw	$t0, -24($fp)
	lw	$t2, -20($fp)
	lw	$t3, -24($fp)
	slt	$t0, $t2, $t3
	xori	$t0, $t0, 1
	sw	$t0, -28($fp)
	lw	$t0, -28($fp)
	beq	$t0, $0, endLoop1
	la	$t0, boardG
	sw	$t0, -32($fp)
	lw	$t0, 0($fp)
	sw	$t0, -36($fp)
	addiu	$t0, $0, 4
	sw	$t0, -40($fp)
	lw	$t2, -40($fp)
	lw	$t3, -36($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -36($fp)
	lw	$t2, -36($fp)
	lw	$t3, -32($fp)
	add	$t0, $t2, $t3
	sw	$t0, -44($fp)
	lw	$t0, -44($fp)
	lw	$t1, ($t0)
	sw	$t1, -48($fp)
	lw	$t0, -48($fp)
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	sw	$t0, -52($fp)
	lw	$t0, 8($fp)
	sw	$t0, -56($fp)
	lw	$t2, -52($fp)
	lw	$t3, -56($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -60($fp)
	lw	$t0, -60($fp)
	beq	$t0, $0, endIf2
	addiu	$t0, $0, 0
	sw	$t0, -64($fp)
	lw	$t0, -64($fp)
	sw	$t0, 12($fp)
	j	checkFend
endIf2:
	lw	$t0, -4($fp)
	sw	$t0, -68($fp)
	lw	$t0, 8($fp)
	sw	$t0, -72($fp)
	lw	$t2, -68($fp)
	lw	$t3, -72($fp)
	slt	$t0, $t3, $t2
	sw	$t0, -76($fp)
	lw	$t0, -76($fp)
	beq	$t0, $0, lazyFalse3
	lw	$t0, 4($fp)
	sw	$t0, -80($fp)
	lw	$t0, 0($fp)
	sw	$t0, -84($fp)
	lw	$t2, -80($fp)
	lw	$t3, -84($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -88($fp)
	lw	$t0, -4($fp)
	sw	$t0, -92($fp)
	lw	$t0, 8($fp)
	sw	$t0, -96($fp)
	lw	$t2, -92($fp)
	lw	$t3, -96($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -100($fp)
	lw	$t2, -88($fp)
	lw	$t3, -100($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -104($fp)
	lw	$t0, -104($fp)
	beq	$t0, $0, lazyFalse3
	addiu	$t0, $0, 1
	sw	$t0, -108($fp)
	j	lazyDone3
lazyFalse3:
	addiu	$t0, $0, 0
	sw	$t0, -108($fp)
lazyDone3:
	lw	$t0, -108($fp)
	beq	$t0, $0, endIf4
	addiu	$t0, $0, 0
	sw	$t0, -112($fp)
	lw	$t0, -112($fp)
	sw	$t0, 12($fp)
	j	checkFend
endIf4:
	lw	$t0, 4($fp)
	sw	$t0, -116($fp)
	lw	$t0, 0($fp)
	sw	$t0, -120($fp)
	lw	$t2, -116($fp)
	lw	$t3, -120($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -124($fp)
	lw	$t0, 8($fp)
	sw	$t0, -128($fp)
	lw	$t0, -4($fp)
	sw	$t0, -132($fp)
	lw	$t2, -128($fp)
	lw	$t3, -132($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -136($fp)
	lw	$t2, -124($fp)
	lw	$t3, -136($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -140($fp)
	lw	$t0, -140($fp)
	beq	$t0, $0, endIf5
	addiu	$t0, $0, 0
	sw	$t0, -144($fp)
	lw	$t0, -144($fp)
	sw	$t0, 12($fp)
	j	checkFend
endIf5:
	lw	$t0, 0($fp)
	sw	$t0, -148($fp)
	addiu	$t0, $0, 1
	sw	$t0, -152($fp)
	lw	$t2, -148($fp)
	lw	$t3, -152($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -156($fp)
	lw	$t0, -156($fp)
	sw	$t0, 0($fp)
	j	loop1
endLoop1:
	addiu	$t0, $0, 1
	sw	$t0, -160($fp)
	lw	$t0, -160($fp)
	sw	$t0, 12($fp)
	j	checkFend
checkFend:
	addi	$sp, $sp, 164
	jr	$ra
queenF:	move	$fp, $sp
	addi	$sp, $sp, -104
	lw	$t0, 4($fp)
	sw	$t0, 0($fp)
	la	$t0, nG
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	lw	$t1, ($t0)
	sw	$t1, -4($fp)
	lw	$t2, 0($fp)
	lw	$t3, -4($fp)
	slt	$t0, $t2, $t3
	xori	$t0, $t0, 1
	sw	$t0, -8($fp)
	lw	$t0, -8($fp)
	beq	$t0, $0, endIf6
	addiu	$t0, $0, 1
	sw	$t0, -12($fp)
	lw	$t0, -12($fp)
	sw	$t0, 12($fp)
	j	queenFend
endIf6:
loop7:
	lw	$t0, 8($fp)
	sw	$t0, -16($fp)
	la	$t0, nG
	sw	$t0, -20($fp)
	lw	$t0, -20($fp)
	lw	$t1, ($t0)
	sw	$t1, -20($fp)
	lw	$t2, -16($fp)
	lw	$t3, -20($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -24($fp)
	lw	$t0, -24($fp)
	beq	$t0, $0, endLoop7
	la	$t0, boardG
	sw	$t0, -28($fp)
	lw	$t0, 4($fp)
	sw	$t0, -32($fp)
	addiu	$t0, $0, 4
	sw	$t0, -36($fp)
	lw	$t2, -36($fp)
	lw	$t3, -32($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -32($fp)
	lw	$t2, -28($fp)
	lw	$t3, -32($fp)
	add	$t0, $t2, $t3
	sw	$t0, -40($fp)
	lw	$t0, 8($fp)
	sw	$t0, -44($fp)
	lw	$t0, -44($fp)
	lw	$t1, -40($fp)
	sw	$t0, ($t1)
	lw	$t0, 4($fp)
	sw	$t0, -48($fp)
	lw	$t0, 8($fp)
	sw	$t0, -52($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -20
	lw	$t0, -48($fp)
	sw	$t0, 4($sp)
	lw	$t0, -52($fp)
	sw	$t0, 8($sp)
	jal	checkF
	addi	$sp, $sp, 20
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -56($fp)
	lw	$t0, -56($fp)
	beq	$t0, $0, lazyFalse8
	lw	$t0, 4($fp)
	sw	$t0, -60($fp)
	addiu	$t0, $0, 1
	sw	$t0, -64($fp)
	lw	$t2, -60($fp)
	lw	$t3, -64($fp)
	add	$t0, $t2, $t3
	sw	$t0, -68($fp)
	addiu	$t0, $0, 0
	sw	$t0, -72($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -20
	lw	$t0, -68($fp)
	sw	$t0, 4($sp)
	lw	$t0, -72($fp)
	sw	$t0, 8($sp)
	jal	queenF
	addi	$sp, $sp, 20
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -76($fp)
	lw	$t0, -76($fp)
	beq	$t0, $0, lazyFalse8
	addiu	$t0, $0, 1
	sw	$t0, -80($fp)
	j	lazyDone8
lazyFalse8:
	addiu	$t0, $0, 0
	sw	$t0, -80($fp)
lazyDone8:
	lw	$t0, -80($fp)
	beq	$t0, $0, endIf9
	addiu	$t0, $0, 1
	sw	$t0, -84($fp)
	lw	$t0, -84($fp)
	sw	$t0, 12($fp)
	j	queenFend
endIf9:
	lw	$t0, 8($fp)
	sw	$t0, -88($fp)
	addiu	$t0, $0, 1
	sw	$t0, -92($fp)
	lw	$t2, -88($fp)
	lw	$t3, -92($fp)
	add	$t0, $t2, $t3
	sw	$t0, -96($fp)
	lw	$t0, -96($fp)
	sw	$t0, 8($fp)
	j	loop7
endLoop7:
	addiu	$t0, $0, 0
	sw	$t0, -100($fp)
	lw	$t0, -100($fp)
	sw	$t0, 12($fp)
	j	queenFend
queenFend:
	addi	$sp, $sp, 104
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -28
	la	$t0, nG
	sw	$t0, 0($fp)
	addiu	$t0, $0, 8
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	lw	$t1, 0($fp)
	sw	$t0, ($t1)
	addiu	$t0, $0, 0
	sw	$t0, -8($fp)
	addiu	$t0, $0, 0
	sw	$t0, -12($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -20
	lw	$t0, -8($fp)
	sw	$t0, 4($sp)
	lw	$t0, -12($fp)
	sw	$t0, 8($sp)
	jal	queenF
	addi	$sp, $sp, 20
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -16($fp)
	la	$t0, boardG
	sw	$t0, -20($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -20($fp)
	sw	$t0, 4($sp)
	jal	printboardF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -24($fp)
mainFend:
	addi	$sp, $sp, 28
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
