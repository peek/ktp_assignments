	.data
eolG:	.space	4
nG:	.space	4
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
sortF:	move	$fp, $sp
	addi	$sp, $sp, -288
	lw	$t0, 8($fp)
	sw	$t0, -16($fp)
	lw	$t0, -16($fp)
	sw	$t0, 0($fp)
	lw	$t0, 12($fp)
	sw	$t0, -20($fp)
	lw	$t0, -20($fp)
	sw	$t0, -4($fp)
	lw	$t0, 8($fp)
	sw	$t0, -24($fp)
	lw	$t0, 12($fp)
	sw	$t0, -28($fp)
	lw	$t2, -24($fp)
	lw	$t3, -28($fp)
	add	$t0, $t2, $t3
	sw	$t0, -32($fp)
	addiu	$t0, $0, 2
	sw	$t0, -36($fp)
	lw	$t2, -32($fp)
	lw	$t3, -36($fp)
	div	$t2, $t3
	mflo	$t0
	sw	$t0, -40($fp)
	lw	$t2, -40($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -44($fp)
	lw	$t0, -44($fp)
	lb	$t1, ($t0)
	sw	$t1, -48($fp)
	la	$t0, dummy
	sw	$t0, -52($fp)
	lw	$t0, -48($fp)
	lw	$t1, -52($fp)
	sb	$t0, ($t1)
	lw	$t0, -52($fp)
	lb	$t1, ($t0)
	sw	$t1, -48($fp)
	lw	$t0, -48($fp)
	sw	$t0, -8($fp)
loop0:
	lw	$t0, 0($fp)
	sw	$t0, -56($fp)
	lw	$t0, -4($fp)
	sw	$t0, -60($fp)
	lw	$t2, -56($fp)
	lw	$t3, -60($fp)
	slt	$t0, $t3, $t2
	xori	$t0, $t0, 1
	sw	$t0, -64($fp)
	lw	$t0, -64($fp)
	beq	$t0, $0, endLoop0
loop1:
	lw	$t0, 0($fp)
	sw	$t0, -68($fp)
	lw	$t2, -68($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -72($fp)
	lw	$t0, -72($fp)
	lb	$t1, ($t0)
	sw	$t1, -76($fp)
	lw	$t0, -8($fp)
	sw	$t0, -80($fp)
	lw	$t2, -76($fp)
	lw	$t3, -80($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -84($fp)
	lw	$t0, -84($fp)
	beq	$t0, $0, endLoop1
	lw	$t0, 0($fp)
	sw	$t0, -88($fp)
	addiu	$t0, $0, 1
	sw	$t0, -92($fp)
	lw	$t2, -88($fp)
	lw	$t3, -92($fp)
	add	$t0, $t2, $t3
	sw	$t0, -96($fp)
	lw	$t0, -96($fp)
	sw	$t0, 0($fp)
	j	loop1
endLoop1:
loop2:
	lw	$t0, -8($fp)
	sw	$t0, -100($fp)
	lw	$t0, -4($fp)
	sw	$t0, -104($fp)
	lw	$t2, -104($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -108($fp)
	lw	$t0, -108($fp)
	lb	$t1, ($t0)
	sw	$t1, -112($fp)
	lw	$t2, -100($fp)
	lw	$t3, -112($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -116($fp)
	lw	$t0, -116($fp)
	beq	$t0, $0, endLoop2
	lw	$t0, -4($fp)
	sw	$t0, -120($fp)
	addiu	$t0, $0, 1
	sw	$t0, -124($fp)
	lw	$t2, -120($fp)
	lw	$t3, -124($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -128($fp)
	lw	$t0, -128($fp)
	sw	$t0, -4($fp)
	j	loop2
endLoop2:
	lw	$t0, 0($fp)
	sw	$t0, -132($fp)
	lw	$t0, -4($fp)
	sw	$t0, -136($fp)
	lw	$t2, -132($fp)
	lw	$t3, -136($fp)
	slt	$t0, $t3, $t2
	xori	$t0, $t0, 1
	sw	$t0, -140($fp)
	lw	$t0, -140($fp)
	beq	$t0, $0, endIf3
	lw	$t0, 0($fp)
	sw	$t0, -144($fp)
	lw	$t2, -144($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -148($fp)
	lw	$t0, -148($fp)
	lb	$t1, ($t0)
	sw	$t1, -152($fp)
	la	$t0, dummy
	sw	$t0, -156($fp)
	lw	$t0, -152($fp)
	lw	$t1, -156($fp)
	sb	$t0, ($t1)
	lw	$t0, -156($fp)
	lb	$t1, ($t0)
	sw	$t1, -152($fp)
	lw	$t0, -152($fp)
	sw	$t0, -12($fp)
	lw	$t0, 0($fp)
	sw	$t0, -160($fp)
	lw	$t2, 4($fp)
	lw	$t3, -160($fp)
	add	$t0, $t2, $t3
	sw	$t0, -164($fp)
	lw	$t0, -4($fp)
	sw	$t0, -168($fp)
	lw	$t2, -168($fp)
	lw	$t3, 4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -172($fp)
	lw	$t0, -172($fp)
	lb	$t1, ($t0)
	sw	$t1, -176($fp)
	lw	$t0, -176($fp)
	lw	$t1, -164($fp)
	sb	$t0, ($t1)
	lw	$t0, -164($fp)
	lb	$t1, ($t0)
	sw	$t1, -176($fp)
	lw	$t0, -4($fp)
	sw	$t0, -180($fp)
	lw	$t2, 4($fp)
	lw	$t3, -180($fp)
	add	$t0, $t2, $t3
	sw	$t0, -184($fp)
	lw	$t0, -12($fp)
	sw	$t0, -188($fp)
	lw	$t0, -188($fp)
	lw	$t1, -184($fp)
	sb	$t0, ($t1)
	lw	$t0, -184($fp)
	lb	$t1, ($t0)
	sw	$t1, -188($fp)
	lw	$t0, 0($fp)
	sw	$t0, -192($fp)
	addiu	$t0, $0, 1
	sw	$t0, -196($fp)
	lw	$t2, -192($fp)
	lw	$t3, -196($fp)
	add	$t0, $t2, $t3
	sw	$t0, -200($fp)
	lw	$t0, -200($fp)
	sw	$t0, 0($fp)
	lw	$t0, -4($fp)
	sw	$t0, -204($fp)
	addiu	$t0, $0, 1
	sw	$t0, -208($fp)
	lw	$t2, -204($fp)
	lw	$t3, -208($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -212($fp)
	lw	$t0, -212($fp)
	sw	$t0, -4($fp)
endIf3:
	j	loop0
endLoop0:
	lw	$t0, 4($fp)
	sw	$t0, -216($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -216($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -220($fp)
	la	$t0, eolG
	sw	$t0, -224($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -224($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -228($fp)
	lw	$t0, 8($fp)
	sw	$t0, -232($fp)
	lw	$t0, -4($fp)
	sw	$t0, -236($fp)
	lw	$t2, -232($fp)
	lw	$t3, -236($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -240($fp)
	lw	$t0, -240($fp)
	beq	$t0, $0, endIf4
	lw	$t0, 4($fp)
	sw	$t0, -244($fp)
	lw	$t0, 8($fp)
	sw	$t0, -248($fp)
	lw	$t0, -4($fp)
	sw	$t0, -252($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -24
	lw	$t0, -244($fp)
	sw	$t0, 4($sp)
	lw	$t0, -248($fp)
	sw	$t0, 8($sp)
	lw	$t0, -252($fp)
	sw	$t0, 12($sp)
	jal	sortF
	addi	$sp, $sp, 24
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -256($fp)
endIf4:
	lw	$t0, 0($fp)
	sw	$t0, -260($fp)
	lw	$t0, 12($fp)
	sw	$t0, -264($fp)
	lw	$t2, -260($fp)
	lw	$t3, -264($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -268($fp)
	lw	$t0, -268($fp)
	beq	$t0, $0, endIf5
	lw	$t0, 4($fp)
	sw	$t0, -272($fp)
	lw	$t0, 0($fp)
	sw	$t0, -276($fp)
	lw	$t0, 12($fp)
	sw	$t0, -280($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -24
	lw	$t0, -272($fp)
	sw	$t0, 4($sp)
	lw	$t0, -276($fp)
	sw	$t0, 8($sp)
	lw	$t0, -280($fp)
	sw	$t0, 12($sp)
	jal	sortF
	addi	$sp, $sp, 24
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -284($fp)
endIf5:
sortFend:
	addi	$sp, $sp, 288
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -248
	addiu	$t0, $0, 24
	sw	$t0, -28($fp)
	lw	$t3, -28($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -28($fp)
	la	$t0, eolG
	sw	$t0, -44($fp)
	addiu	$t0, $0, 0
	sw	$t0, -48($fp)
	lw	$t2, -44($fp)
	lw	$t3, -48($fp)
	add	$t0, $t2, $t3
	sw	$t0, -52($fp)
	addiu	$t0, $0, 10
	sw	$t0, -56($fp)
	lw	$t0, -56($fp)
	lw	$t1, -52($fp)
	sb	$t0, ($t1)
	lw	$t0, -52($fp)
	lb	$t1, ($t0)
	sw	$t1, -56($fp)
	la	$t0, eolG
	sw	$t0, -60($fp)
	addiu	$t0, $0, 1
	sw	$t0, -64($fp)
	lw	$t2, -60($fp)
	lw	$t3, -64($fp)
	add	$t0, $t2, $t3
	sw	$t0, -68($fp)
	addiu	$t0, $0, 0
	sw	$t0, -72($fp)
	lw	$t0, -72($fp)
	lw	$t1, -68($fp)
	sb	$t0, ($t1)
	lw	$t0, -68($fp)
	lb	$t1, ($t0)
	sw	$t1, -72($fp)
	la	$t0, nG
	sw	$t0, -76($fp)
	addiu	$t0, $0, 26
	sw	$t0, -80($fp)
	lw	$t0, -80($fp)
	lw	$t1, -76($fp)
	sw	$t0, ($t1)
	la	$t0, nG
	sw	$t0, -84($fp)
	lw	$t0, -84($fp)
	lw	$t1, ($t0)
	sw	$t1, -84($fp)
	lw	$t2, -28($fp)
	lw	$t3, -84($fp)
	add	$t0, $t2, $t3
	sw	$t0, -88($fp)
	addiu	$t0, $0, 0
	sw	$t0, -92($fp)
	lw	$t0, -92($fp)
	lw	$t1, -88($fp)
	sb	$t0, ($t1)
	lw	$t0, -88($fp)
	lb	$t1, ($t0)
	sw	$t1, -92($fp)
	addiu	$t0, $0, 0
	sw	$t0, -96($fp)
	lw	$t0, -96($fp)
	sw	$t0, -32($fp)
	addiu	$t0, $0, 11
	sw	$t0, -100($fp)
	lw	$t0, -100($fp)
	sw	$t0, -40($fp)
loop6:
	lw	$t0, -32($fp)
	sw	$t0, -104($fp)
	la	$t0, nG
	sw	$t0, -108($fp)
	lw	$t0, -108($fp)
	lw	$t1, ($t0)
	sw	$t1, -108($fp)
	lw	$t2, -104($fp)
	lw	$t3, -108($fp)
	slt	$t0, $t2, $t3
	sw	$t0, -112($fp)
	lw	$t0, -112($fp)
	beq	$t0, $0, endLoop6
	lw	$t0, -40($fp)
	sw	$t0, -116($fp)
	lw	$t0, -40($fp)
	sw	$t0, -120($fp)
	addiu	$t0, $0, 26
	sw	$t0, -124($fp)
	lw	$t2, -120($fp)
	lw	$t3, -124($fp)
	div	$t2, $t3
	mflo	$t0
	sw	$t0, -128($fp)
	addiu	$t0, $0, 26
	sw	$t0, -132($fp)
	lw	$t2, -128($fp)
	lw	$t3, -132($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -136($fp)
	lw	$t2, -116($fp)
	lw	$t3, -136($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -140($fp)
	la	$t0, dummy
	sw	$t0, -144($fp)
	lw	$t0, -140($fp)
	lw	$t1, -144($fp)
	sb	$t0, ($t1)
	lw	$t0, -144($fp)
	lb	$t1, ($t0)
	sw	$t1, -140($fp)
	lw	$t0, -140($fp)
	sw	$t0, -36($fp)
	lw	$t0, -32($fp)
	sw	$t0, -148($fp)
	lw	$t2, -28($fp)
	lw	$t3, -148($fp)
	add	$t0, $t2, $t3
	sw	$t0, -152($fp)
	addiu	$t0, $0, 97
	sw	$t0, -156($fp)
	lw	$t0, -36($fp)
	sw	$t0, -160($fp)
	lw	$t2, -156($fp)
	lw	$t3, -160($fp)
	add	$t0, $t2, $t3
	sw	$t0, -164($fp)
	lw	$t0, -164($fp)
	lw	$t1, -152($fp)
	sb	$t0, ($t1)
	lw	$t0, -152($fp)
	lb	$t1, ($t0)
	sw	$t1, -164($fp)
	lw	$t0, -32($fp)
	sw	$t0, -168($fp)
	addiu	$t0, $0, 1
	sw	$t0, -172($fp)
	lw	$t2, -168($fp)
	lw	$t3, -172($fp)
	add	$t0, $t2, $t3
	sw	$t0, -176($fp)
	lw	$t0, -176($fp)
	sw	$t0, -32($fp)
	lw	$t0, -40($fp)
	sw	$t0, -180($fp)
	addiu	$t0, $0, 17
	sw	$t0, -184($fp)
	lw	$t2, -180($fp)
	lw	$t3, -184($fp)
	add	$t0, $t2, $t3
	sw	$t0, -188($fp)
	lw	$t0, -188($fp)
	sw	$t0, -40($fp)
	j	loop6
endLoop6:
	lw	$t0, -28($fp)
	sw	$t0, -192($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -192($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -196($fp)
	la	$t0, eolG
	sw	$t0, -200($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -200($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -204($fp)
	lw	$t0, -28($fp)
	sw	$t0, -208($fp)
	addiu	$t0, $0, 0
	sw	$t0, -212($fp)
	la	$t0, nG
	sw	$t0, -216($fp)
	lw	$t0, -216($fp)
	lw	$t1, ($t0)
	sw	$t1, -216($fp)
	addiu	$t0, $0, 1
	sw	$t0, -220($fp)
	lw	$t2, -216($fp)
	lw	$t3, -220($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -224($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -24
	lw	$t0, -208($fp)
	sw	$t0, 4($sp)
	lw	$t0, -212($fp)
	sw	$t0, 8($sp)
	lw	$t0, -224($fp)
	sw	$t0, 12($sp)
	jal	sortF
	addi	$sp, $sp, 24
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -228($fp)
	lw	$t0, -28($fp)
	sw	$t0, -232($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -232($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -236($fp)
	la	$t0, eolG
	sw	$t0, -240($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -240($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -244($fp)
mainFend:
	addi	$sp, $sp, 248
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
