	.data
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
fF:	move	$fp, $sp
	addi	$sp, $sp, -12
	lw	$t0, 4($fp)
	sw	$t0, 0($fp)
	lw	$t0, 8($fp)
	sw	$t0, -4($fp)
	lw	$t2, 0($fp)
	lw	$t3, -4($fp)
	add	$t0, $t2, $t3
	sw	$t0, -8($fp)
	lw	$t0, -8($fp)
	sw	$t0, 12($fp)
	j	fFend
fFend:
	addi	$sp, $sp, 12
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -12
	addiu	$t0, $0, 2
	sw	$t0, 0($fp)
	addiu	$t0, $0, 3
	sw	$t0, -4($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -20
	lw	$t0, 0($fp)
	sw	$t0, 4($sp)
	lw	$t0, -4($fp)
	sw	$t0, 8($sp)
	jal	fF
	addi	$sp, $sp, 20
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -8($fp)
mainFend:
	addi	$sp, $sp, 12
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
