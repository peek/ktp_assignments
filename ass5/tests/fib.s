	.data
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
fibF:	move	$fp, $sp
	addi	$sp, $sp, -68
	lw	$t0, 4($fp)
	sw	$t0, 0($fp)
	addiu	$t0, $0, 0
	sw	$t0, -4($fp)
	lw	$t2, 0($fp)
	lw	$t3, -4($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -8($fp)
	lw	$t0, -8($fp)
	beq	$t0, $0, endIf0
	addiu	$t0, $0, 1
	sw	$t0, -12($fp)
	lw	$t0, -12($fp)
	sw	$t0, 8($fp)
	j	fibFend
endIf0:
	lw	$t0, 4($fp)
	sw	$t0, -16($fp)
	addiu	$t0, $0, 1
	sw	$t0, -20($fp)
	lw	$t2, -16($fp)
	lw	$t3, -20($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -24($fp)
	lw	$t0, -24($fp)
	beq	$t0, $0, endIf1
	addiu	$t0, $0, 1
	sw	$t0, -28($fp)
	lw	$t0, -28($fp)
	sw	$t0, 8($fp)
	j	fibFend
endIf1:
	lw	$t0, 4($fp)
	sw	$t0, -32($fp)
	addiu	$t0, $0, 1
	sw	$t0, -36($fp)
	lw	$t2, -32($fp)
	lw	$t3, -36($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -40($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -40($fp)
	sw	$t0, 4($sp)
	jal	fibF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -44($fp)
	lw	$t0, 4($fp)
	sw	$t0, -48($fp)
	addiu	$t0, $0, 2
	sw	$t0, -52($fp)
	lw	$t2, -48($fp)
	lw	$t3, -52($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -56($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -56($fp)
	sw	$t0, 4($sp)
	jal	fibF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -60($fp)
	lw	$t2, -44($fp)
	lw	$t3, -60($fp)
	add	$t0, $t2, $t3
	sw	$t0, -64($fp)
	lw	$t0, -64($fp)
	sw	$t0, 8($fp)
	j	fibFend
fibFend:
	addi	$sp, $sp, 68
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -132
	addiu	$t0, $0, 0
	sw	$t0, -12($fp)
	lw	$t3, -12($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -12($fp)
	addiu	$t0, $0, 4
	sw	$t0, -16($fp)
	lw	$t3, -16($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -16($fp)
	addiu	$t0, $0, 0
	sw	$t0, -20($fp)
	lw	$t2, -12($fp)
	lw	$t3, -20($fp)
	add	$t0, $t2, $t3
	sw	$t0, -24($fp)
	addiu	$t0, $0, 32
	sw	$t0, -28($fp)
	lw	$t0, -28($fp)
	lw	$t1, -24($fp)
	sb	$t0, ($t1)
	lw	$t0, -24($fp)
	lb	$t1, ($t0)
	sw	$t1, -28($fp)
	addiu	$t0, $0, 1
	sw	$t0, -32($fp)
	lw	$t2, -12($fp)
	lw	$t3, -32($fp)
	add	$t0, $t2, $t3
	sw	$t0, -36($fp)
	addiu	$t0, $0, 0
	sw	$t0, -40($fp)
	lw	$t0, -40($fp)
	lw	$t1, -36($fp)
	sb	$t0, ($t1)
	lw	$t0, -36($fp)
	lb	$t1, ($t0)
	sw	$t1, -40($fp)
	addiu	$t0, $0, 0
	sw	$t0, -44($fp)
	lw	$t2, -16($fp)
	lw	$t3, -44($fp)
	add	$t0, $t2, $t3
	sw	$t0, -48($fp)
	addiu	$t0, $0, 10
	sw	$t0, -52($fp)
	lw	$t0, -52($fp)
	lw	$t1, -48($fp)
	sb	$t0, ($t1)
	lw	$t0, -48($fp)
	lb	$t1, ($t0)
	sw	$t1, -52($fp)
	addiu	$t0, $0, 1
	sw	$t0, -56($fp)
	lw	$t2, -16($fp)
	lw	$t3, -56($fp)
	add	$t0, $t2, $t3
	sw	$t0, -60($fp)
	addiu	$t0, $0, 0
	sw	$t0, -64($fp)
	lw	$t0, -64($fp)
	lw	$t1, -60($fp)
	sb	$t0, ($t1)
	lw	$t0, -60($fp)
	lb	$t1, ($t0)
	sw	$t1, -64($fp)
	addiu	$t0, $0, 0
	sw	$t0, -68($fp)
	lw	$t0, -68($fp)
	sw	$t0, -8($fp)
loop2:
	lw	$t0, -8($fp)
	sw	$t0, -72($fp)
	addiu	$t0, $0, 12
	sw	$t0, -76($fp)
	lw	$t2, -72($fp)
	lw	$t3, -76($fp)
	slt	$t0, $t3, $t2
	xori	$t0, $t0, 1
	sw	$t0, -80($fp)
	lw	$t0, -80($fp)
	beq	$t0, $0, endLoop2
	lw	$t0, -8($fp)
	sw	$t0, -84($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -84($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -88($fp)
	lw	$t0, -12($fp)
	sw	$t0, -92($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -92($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -96($fp)
	lw	$t0, -8($fp)
	sw	$t0, -100($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -100($fp)
	sw	$t0, 4($sp)
	jal	fibF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -104($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -104($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -108($fp)
	lw	$t0, -16($fp)
	sw	$t0, -112($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -112($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -116($fp)
	lw	$t0, -8($fp)
	sw	$t0, -120($fp)
	addiu	$t0, $0, 1
	sw	$t0, -124($fp)
	lw	$t2, -120($fp)
	lw	$t3, -124($fp)
	add	$t0, $t2, $t3
	sw	$t0, -128($fp)
	lw	$t0, -128($fp)
	sw	$t0, -8($fp)
	j	loop2
endLoop2:
mainFend:
	addi	$sp, $sp, 132
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
