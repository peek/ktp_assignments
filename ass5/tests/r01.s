	.data
xG:	.space	4
yG:	.space	4
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -36
	la	$t0, xG
	sw	$t0, -8($fp)
	addiu	$t0, $0, 42
	sw	$t0, -12($fp)
	lw	$t0, -12($fp)
	lw	$t1, -8($fp)
	sw	$t0, ($t1)
	la	$t0, yG
	sw	$t0, -16($fp)
	addiu	$t0, $0, 43
	sw	$t0, -20($fp)
	lw	$t0, -20($fp)
	lw	$t1, -16($fp)
	sb	$t0, ($t1)
	lw	$t0, -16($fp)
	lb	$t1, ($t0)
	sw	$t1, -20($fp)
	addiu	$t0, $0, 65
	sw	$t0, -24($fp)
	lw	$t0, -24($fp)
	sw	$t0, 0($fp)
	addiu	$t0, $0, 10
	sw	$t0, -28($fp)
	la	$t0, dummy
	sw	$t0, -32($fp)
	lw	$t0, -28($fp)
	lw	$t1, -32($fp)
	sb	$t0, ($t1)
	lw	$t0, -32($fp)
	lb	$t1, ($t0)
	sw	$t1, -28($fp)
	lw	$t0, -28($fp)
	sw	$t0, -4($fp)
mainFend:
	addi	$sp, $sp, 36
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
