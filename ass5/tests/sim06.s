	.data
tG:	.space	4
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -104
	addiu	$t0, $0, 10
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	sw	$t0, 0($fp)
	la	$t0, tG
	sw	$t0, -8($fp)
	addiu	$t0, $0, 1
	sw	$t0, -12($fp)
	lw	$t2, -8($fp)
	lw	$t3, -12($fp)
	add	$t0, $t2, $t3
	sw	$t0, -16($fp)
	addiu	$t0, $0, 0
	sw	$t0, -20($fp)
	lw	$t0, -20($fp)
	lw	$t1, -16($fp)
	sb	$t0, ($t1)
	lw	$t0, -16($fp)
	lb	$t1, ($t0)
	sw	$t1, -20($fp)
loop0:
	lw	$t0, 0($fp)
	sw	$t0, -24($fp)
	lw	$t0, -24($fp)
	beq	$t0, $0, endLoop0
	la	$t0, tG
	sw	$t0, -28($fp)
	addiu	$t0, $0, 0
	sw	$t0, -32($fp)
	lw	$t2, -28($fp)
	lw	$t3, -32($fp)
	add	$t0, $t2, $t3
	sw	$t0, -36($fp)
	addiu	$t0, $0, 48
	sw	$t0, -40($fp)
	lw	$t0, 0($fp)
	sw	$t0, -44($fp)
	lw	$t2, -40($fp)
	lw	$t3, -44($fp)
	add	$t0, $t2, $t3
	sw	$t0, -48($fp)
	addiu	$t0, $0, 1
	sw	$t0, -52($fp)
	lw	$t2, -48($fp)
	lw	$t3, -52($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -56($fp)
	lw	$t0, -56($fp)
	lw	$t1, -36($fp)
	sb	$t0, ($t1)
	lw	$t0, -36($fp)
	lb	$t1, ($t0)
	sw	$t1, -56($fp)
	la	$t0, tG
	sw	$t0, -60($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -60($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -64($fp)
	lw	$t0, 0($fp)
	sw	$t0, -68($fp)
	addiu	$t0, $0, 1
	sw	$t0, -72($fp)
	lw	$t2, -68($fp)
	lw	$t3, -72($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -76($fp)
	lw	$t0, -76($fp)
	sw	$t0, 0($fp)
	j	loop0
endLoop0:
	la	$t0, tG
	sw	$t0, -80($fp)
	addiu	$t0, $0, 0
	sw	$t0, -84($fp)
	lw	$t2, -80($fp)
	lw	$t3, -84($fp)
	add	$t0, $t2, $t3
	sw	$t0, -88($fp)
	addiu	$t0, $0, 10
	sw	$t0, -92($fp)
	lw	$t0, -92($fp)
	lw	$t1, -88($fp)
	sb	$t0, ($t1)
	lw	$t0, -88($fp)
	lb	$t1, ($t0)
	sw	$t1, -92($fp)
	la	$t0, tG
	sw	$t0, -96($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -96($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -100($fp)
mainFend:
	addi	$sp, $sp, 104
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
