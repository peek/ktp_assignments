	.data
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -308
	addiu	$t0, $0, 0
	sw	$t0, -24($fp)
	lw	$t3, -24($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -24($fp)
	addiu	$t0, $0, 4
	sw	$t0, -28($fp)
	lw	$t3, -28($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -28($fp)
	addiu	$t0, $0, 8
	sw	$t0, -32($fp)
	lw	$t3, -32($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -32($fp)
	addiu	$t0, $0, 12
	sw	$t0, -36($fp)
	lw	$t3, -36($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -36($fp)
	addiu	$t0, $0, 16
	sw	$t0, -40($fp)
	lw	$t3, -40($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -40($fp)
	addiu	$t0, $0, 0
	sw	$t0, -44($fp)
	lw	$t2, -24($fp)
	lw	$t3, -44($fp)
	add	$t0, $t2, $t3
	sw	$t0, -48($fp)
	addiu	$t0, $0, 32
	sw	$t0, -52($fp)
	lw	$t0, -52($fp)
	lw	$t1, -48($fp)
	sb	$t0, ($t1)
	lw	$t0, -48($fp)
	lb	$t1, ($t0)
	sw	$t1, -52($fp)
	addiu	$t0, $0, 1
	sw	$t0, -56($fp)
	lw	$t2, -24($fp)
	lw	$t3, -56($fp)
	add	$t0, $t2, $t3
	sw	$t0, -60($fp)
	addiu	$t0, $0, 0
	sw	$t0, -64($fp)
	lw	$t0, -64($fp)
	lw	$t1, -60($fp)
	sb	$t0, ($t1)
	lw	$t0, -60($fp)
	lb	$t1, ($t0)
	sw	$t1, -64($fp)
	addiu	$t0, $0, 0
	sw	$t0, -68($fp)
	lw	$t2, -28($fp)
	lw	$t3, -68($fp)
	add	$t0, $t2, $t3
	sw	$t0, -72($fp)
	addiu	$t0, $0, 88
	sw	$t0, -76($fp)
	lw	$t0, -76($fp)
	lw	$t1, -72($fp)
	sb	$t0, ($t1)
	lw	$t0, -72($fp)
	lb	$t1, ($t0)
	sw	$t1, -76($fp)
	addiu	$t0, $0, 1
	sw	$t0, -80($fp)
	lw	$t2, -28($fp)
	lw	$t3, -80($fp)
	add	$t0, $t2, $t3
	sw	$t0, -84($fp)
	addiu	$t0, $0, 0
	sw	$t0, -88($fp)
	lw	$t0, -88($fp)
	lw	$t1, -84($fp)
	sb	$t0, ($t1)
	lw	$t0, -84($fp)
	lb	$t1, ($t0)
	sw	$t1, -88($fp)
	addiu	$t0, $0, 0
	sw	$t0, -92($fp)
	lw	$t2, -32($fp)
	lw	$t3, -92($fp)
	add	$t0, $t2, $t3
	sw	$t0, -96($fp)
	addiu	$t0, $0, 89
	sw	$t0, -100($fp)
	lw	$t0, -100($fp)
	lw	$t1, -96($fp)
	sb	$t0, ($t1)
	lw	$t0, -96($fp)
	lb	$t1, ($t0)
	sw	$t1, -100($fp)
	addiu	$t0, $0, 1
	sw	$t0, -104($fp)
	lw	$t2, -32($fp)
	lw	$t3, -104($fp)
	add	$t0, $t2, $t3
	sw	$t0, -108($fp)
	addiu	$t0, $0, 0
	sw	$t0, -112($fp)
	lw	$t0, -112($fp)
	lw	$t1, -108($fp)
	sb	$t0, ($t1)
	lw	$t0, -108($fp)
	lb	$t1, ($t0)
	sw	$t1, -112($fp)
	addiu	$t0, $0, 0
	sw	$t0, -116($fp)
	lw	$t2, -36($fp)
	lw	$t3, -116($fp)
	add	$t0, $t2, $t3
	sw	$t0, -120($fp)
	addiu	$t0, $0, 87
	sw	$t0, -124($fp)
	lw	$t0, -124($fp)
	lw	$t1, -120($fp)
	sb	$t0, ($t1)
	lw	$t0, -120($fp)
	lb	$t1, ($t0)
	sw	$t1, -124($fp)
	addiu	$t0, $0, 1
	sw	$t0, -128($fp)
	lw	$t2, -36($fp)
	lw	$t3, -128($fp)
	add	$t0, $t2, $t3
	sw	$t0, -132($fp)
	addiu	$t0, $0, 0
	sw	$t0, -136($fp)
	lw	$t0, -136($fp)
	lw	$t1, -132($fp)
	sb	$t0, ($t1)
	lw	$t0, -132($fp)
	lb	$t1, ($t0)
	sw	$t1, -136($fp)
	addiu	$t0, $0, 0
	sw	$t0, -140($fp)
	lw	$t2, -40($fp)
	lw	$t3, -140($fp)
	add	$t0, $t2, $t3
	sw	$t0, -144($fp)
	addiu	$t0, $0, 10
	sw	$t0, -148($fp)
	lw	$t0, -148($fp)
	lw	$t1, -144($fp)
	sb	$t0, ($t1)
	lw	$t0, -144($fp)
	lb	$t1, ($t0)
	sw	$t1, -148($fp)
	addiu	$t0, $0, 1
	sw	$t0, -152($fp)
	lw	$t2, -40($fp)
	lw	$t3, -152($fp)
	add	$t0, $t2, $t3
	sw	$t0, -156($fp)
	addiu	$t0, $0, 0
	sw	$t0, -160($fp)
	lw	$t0, -160($fp)
	lw	$t1, -156($fp)
	sb	$t0, ($t1)
	lw	$t0, -156($fp)
	lb	$t1, ($t0)
	sw	$t1, -160($fp)
	addiu	$t0, $0, 0
	sw	$t0, -164($fp)
	lw	$t0, -164($fp)
	sw	$t0, -20($fp)
loop0:
	lw	$t0, -20($fp)
	sw	$t0, -168($fp)
	addiu	$t0, $0, 21
	sw	$t0, -172($fp)
	lw	$t2, -168($fp)
	lw	$t3, -172($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	sw	$t0, -176($fp)
	lw	$t0, -176($fp)
	beq	$t0, $0, endLoop0
	lw	$t0, -20($fp)
	sw	$t0, -180($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -180($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -184($fp)
	lw	$t0, -24($fp)
	sw	$t0, -188($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -188($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -192($fp)
	lw	$t0, -20($fp)
	sw	$t0, -196($fp)
	addiu	$t0, $0, 2
	sw	$t0, -200($fp)
	lw	$t2, -196($fp)
	lw	$t3, -200($fp)
	div	$t2, $t3
	mflo	$t0
	sw	$t0, -204($fp)
	addiu	$t0, $0, 2
	sw	$t0, -208($fp)
	lw	$t2, -204($fp)
	lw	$t3, -208($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -212($fp)
	lw	$t0, -20($fp)
	sw	$t0, -216($fp)
	lw	$t2, -212($fp)
	lw	$t3, -216($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -220($fp)
	lw	$t0, -220($fp)
	beq	$t0, $0, endIf1
	lw	$t0, -28($fp)
	sw	$t0, -224($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -224($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -228($fp)
endIf1:
	lw	$t0, -20($fp)
	sw	$t0, -232($fp)
	addiu	$t0, $0, 3
	sw	$t0, -236($fp)
	lw	$t2, -232($fp)
	lw	$t3, -236($fp)
	div	$t2, $t3
	mflo	$t0
	sw	$t0, -240($fp)
	addiu	$t0, $0, 3
	sw	$t0, -244($fp)
	lw	$t2, -240($fp)
	lw	$t3, -244($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -248($fp)
	lw	$t0, -20($fp)
	sw	$t0, -252($fp)
	lw	$t2, -248($fp)
	lw	$t3, -252($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -256($fp)
	lw	$t0, -256($fp)
	beq	$t0, $0, else2
	lw	$t0, -32($fp)
	sw	$t0, -260($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -260($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -264($fp)
	j	endIf2
else2:
	lw	$t0, -20($fp)
	sw	$t0, -268($fp)
	addiu	$t0, $0, 10
	sw	$t0, -272($fp)
	lw	$t2, -268($fp)
	lw	$t3, -272($fp)
	slt	$t0, $t3, $t2
	sw	$t0, -276($fp)
	lw	$t0, -276($fp)
	beq	$t0, $0, endIf3
	lw	$t0, -36($fp)
	sw	$t0, -280($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -280($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -284($fp)
endIf3:
endIf2:
	lw	$t0, -40($fp)
	sw	$t0, -288($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -288($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -292($fp)
	lw	$t0, -20($fp)
	sw	$t0, -296($fp)
	addiu	$t0, $0, 1
	sw	$t0, -300($fp)
	lw	$t2, -296($fp)
	lw	$t3, -300($fp)
	add	$t0, $t2, $t3
	sw	$t0, -304($fp)
	lw	$t0, -304($fp)
	sw	$t0, -20($fp)
	j	loop0
endLoop0:
mainFend:
	addi	$sp, $sp, 308
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
