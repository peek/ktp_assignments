	.data
xG:	.space	8
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -264
	addiu	$t0, $0, 8
	sw	$t0, -12($fp)
	lw	$t3, -12($fp)
	sub	$t0, $fp, $t3
	sw	$t0, -12($fp)
	la	$t0, xG
	sw	$t0, -16($fp)
	addiu	$t0, $0, 0
	sw	$t0, -20($fp)
	lw	$t2, -16($fp)
	lw	$t3, -20($fp)
	add	$t0, $t2, $t3
	sw	$t0, -24($fp)
	addiu	$t0, $0, 72
	sw	$t0, -28($fp)
	lw	$t0, -28($fp)
	lw	$t1, -24($fp)
	sb	$t0, ($t1)
	lw	$t0, -24($fp)
	lb	$t1, ($t0)
	sw	$t1, -28($fp)
	la	$t0, xG
	sw	$t0, -32($fp)
	addiu	$t0, $0, 1
	sw	$t0, -36($fp)
	lw	$t2, -32($fp)
	lw	$t3, -36($fp)
	add	$t0, $t2, $t3
	sw	$t0, -40($fp)
	addiu	$t0, $0, 101
	sw	$t0, -44($fp)
	lw	$t0, -44($fp)
	lw	$t1, -40($fp)
	sb	$t0, ($t1)
	lw	$t0, -40($fp)
	lb	$t1, ($t0)
	sw	$t1, -44($fp)
	la	$t0, xG
	sw	$t0, -48($fp)
	addiu	$t0, $0, 2
	sw	$t0, -52($fp)
	lw	$t2, -48($fp)
	lw	$t3, -52($fp)
	add	$t0, $t2, $t3
	sw	$t0, -56($fp)
	addiu	$t0, $0, 108
	sw	$t0, -60($fp)
	lw	$t0, -60($fp)
	lw	$t1, -56($fp)
	sb	$t0, ($t1)
	lw	$t0, -56($fp)
	lb	$t1, ($t0)
	sw	$t1, -60($fp)
	la	$t0, xG
	sw	$t0, -64($fp)
	addiu	$t0, $0, 3
	sw	$t0, -68($fp)
	lw	$t2, -64($fp)
	lw	$t3, -68($fp)
	add	$t0, $t2, $t3
	sw	$t0, -72($fp)
	addiu	$t0, $0, 108
	sw	$t0, -76($fp)
	lw	$t0, -76($fp)
	lw	$t1, -72($fp)
	sb	$t0, ($t1)
	lw	$t0, -72($fp)
	lb	$t1, ($t0)
	sw	$t1, -76($fp)
	la	$t0, xG
	sw	$t0, -80($fp)
	addiu	$t0, $0, 4
	sw	$t0, -84($fp)
	lw	$t2, -80($fp)
	lw	$t3, -84($fp)
	add	$t0, $t2, $t3
	sw	$t0, -88($fp)
	addiu	$t0, $0, 111
	sw	$t0, -92($fp)
	lw	$t0, -92($fp)
	lw	$t1, -88($fp)
	sb	$t0, ($t1)
	lw	$t0, -88($fp)
	lb	$t1, ($t0)
	sw	$t1, -92($fp)
	la	$t0, xG
	sw	$t0, -96($fp)
	addiu	$t0, $0, 5
	sw	$t0, -100($fp)
	lw	$t2, -96($fp)
	lw	$t3, -100($fp)
	add	$t0, $t2, $t3
	sw	$t0, -104($fp)
	addiu	$t0, $0, 10
	sw	$t0, -108($fp)
	lw	$t0, -108($fp)
	lw	$t1, -104($fp)
	sb	$t0, ($t1)
	lw	$t0, -104($fp)
	lb	$t1, ($t0)
	sw	$t1, -108($fp)
	la	$t0, xG
	sw	$t0, -112($fp)
	addiu	$t0, $0, 6
	sw	$t0, -116($fp)
	lw	$t2, -112($fp)
	lw	$t3, -116($fp)
	add	$t0, $t2, $t3
	sw	$t0, -120($fp)
	addiu	$t0, $0, 0
	sw	$t0, -124($fp)
	lw	$t0, -124($fp)
	lw	$t1, -120($fp)
	sb	$t0, ($t1)
	lw	$t0, -120($fp)
	lb	$t1, ($t0)
	sw	$t1, -124($fp)
	addiu	$t0, $0, 0
	sw	$t0, -128($fp)
	lw	$t2, -12($fp)
	lw	$t3, -128($fp)
	add	$t0, $t2, $t3
	sw	$t0, -132($fp)
	addiu	$t0, $0, 71
	sw	$t0, -136($fp)
	lw	$t0, -136($fp)
	lw	$t1, -132($fp)
	sb	$t0, ($t1)
	lw	$t0, -132($fp)
	lb	$t1, ($t0)
	sw	$t1, -136($fp)
	addiu	$t0, $0, 1
	sw	$t0, -140($fp)
	lw	$t2, -12($fp)
	lw	$t3, -140($fp)
	add	$t0, $t2, $t3
	sw	$t0, -144($fp)
	addiu	$t0, $0, 111
	sw	$t0, -148($fp)
	lw	$t0, -148($fp)
	lw	$t1, -144($fp)
	sb	$t0, ($t1)
	lw	$t0, -144($fp)
	lb	$t1, ($t0)
	sw	$t1, -148($fp)
	addiu	$t0, $0, 2
	sw	$t0, -152($fp)
	lw	$t2, -12($fp)
	lw	$t3, -152($fp)
	add	$t0, $t2, $t3
	sw	$t0, -156($fp)
	addiu	$t0, $0, 111
	sw	$t0, -160($fp)
	lw	$t0, -160($fp)
	lw	$t1, -156($fp)
	sb	$t0, ($t1)
	lw	$t0, -156($fp)
	lb	$t1, ($t0)
	sw	$t1, -160($fp)
	addiu	$t0, $0, 3
	sw	$t0, -164($fp)
	lw	$t2, -12($fp)
	lw	$t3, -164($fp)
	add	$t0, $t2, $t3
	sw	$t0, -168($fp)
	addiu	$t0, $0, 100
	sw	$t0, -172($fp)
	lw	$t0, -172($fp)
	lw	$t1, -168($fp)
	sb	$t0, ($t1)
	lw	$t0, -168($fp)
	lb	$t1, ($t0)
	sw	$t1, -172($fp)
	addiu	$t0, $0, 4
	sw	$t0, -176($fp)
	lw	$t2, -12($fp)
	lw	$t3, -176($fp)
	add	$t0, $t2, $t3
	sw	$t0, -180($fp)
	addiu	$t0, $0, 32
	sw	$t0, -184($fp)
	lw	$t0, -184($fp)
	lw	$t1, -180($fp)
	sb	$t0, ($t1)
	lw	$t0, -180($fp)
	lb	$t1, ($t0)
	sw	$t1, -184($fp)
	addiu	$t0, $0, 5
	sw	$t0, -188($fp)
	lw	$t2, -12($fp)
	lw	$t3, -188($fp)
	add	$t0, $t2, $t3
	sw	$t0, -192($fp)
	addiu	$t0, $0, 98
	sw	$t0, -196($fp)
	lw	$t0, -196($fp)
	lw	$t1, -192($fp)
	sb	$t0, ($t1)
	lw	$t0, -192($fp)
	lb	$t1, ($t0)
	sw	$t1, -196($fp)
	addiu	$t0, $0, 6
	sw	$t0, -200($fp)
	lw	$t2, -12($fp)
	lw	$t3, -200($fp)
	add	$t0, $t2, $t3
	sw	$t0, -204($fp)
	addiu	$t0, $0, 121
	sw	$t0, -208($fp)
	lw	$t0, -208($fp)
	lw	$t1, -204($fp)
	sb	$t0, ($t1)
	lw	$t0, -204($fp)
	lb	$t1, ($t0)
	sw	$t1, -208($fp)
	addiu	$t0, $0, 7
	sw	$t0, -212($fp)
	lw	$t2, -12($fp)
	lw	$t3, -212($fp)
	add	$t0, $t2, $t3
	sw	$t0, -216($fp)
	addiu	$t0, $0, 101
	sw	$t0, -220($fp)
	lw	$t0, -220($fp)
	lw	$t1, -216($fp)
	sb	$t0, ($t1)
	lw	$t0, -216($fp)
	lb	$t1, ($t0)
	sw	$t1, -220($fp)
	addiu	$t0, $0, 8
	sw	$t0, -224($fp)
	lw	$t2, -12($fp)
	lw	$t3, -224($fp)
	add	$t0, $t2, $t3
	sw	$t0, -228($fp)
	addiu	$t0, $0, 10
	sw	$t0, -232($fp)
	lw	$t0, -232($fp)
	lw	$t1, -228($fp)
	sb	$t0, ($t1)
	lw	$t0, -228($fp)
	lb	$t1, ($t0)
	sw	$t1, -232($fp)
	addiu	$t0, $0, 9
	sw	$t0, -236($fp)
	lw	$t2, -12($fp)
	lw	$t3, -236($fp)
	add	$t0, $t2, $t3
	sw	$t0, -240($fp)
	addiu	$t0, $0, 0
	sw	$t0, -244($fp)
	lw	$t0, -244($fp)
	lw	$t1, -240($fp)
	sb	$t0, ($t1)
	lw	$t0, -240($fp)
	lb	$t1, ($t0)
	sw	$t1, -244($fp)
	la	$t0, xG
	sw	$t0, -248($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -248($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -252($fp)
	lw	$t0, -12($fp)
	sw	$t0, -256($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -256($fp)
	sw	$t0, 4($sp)
	jal	putstringF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -260($fp)
mainFend:
	addi	$sp, $sp, 264
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
