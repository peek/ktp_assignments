	.data
aG:	.space	4
dummy:	.space	4
	.text
	.globl	main
main:	sw	$ra, ($sp)
	addi	$sp, $sp, -8
	jal	mainF
	addi	$sp, $sp, 8
	lw	$ra, ($sp)
	jr	$ra
facF:	move	$fp, $sp
	addi	$sp, $sp, -40
	lw	$t0, 4($fp)
	sw	$t0, 0($fp)
	addiu	$t0, $0, 0
	sw	$t0, -4($fp)
	lw	$t2, 0($fp)
	lw	$t3, -4($fp)
	slt	$t0, $t2, $t3
	slt	$t1, $t3, $t2
	or	$t0, $t0, $t1
	xori	$t0, $t0, 1
	sw	$t0, -8($fp)
	lw	$t0, -8($fp)
	beq	$t0, $0, else0
	addiu	$t0, $0, 1
	sw	$t0, -12($fp)
	lw	$t0, -12($fp)
	sw	$t0, 8($fp)
	j	facFend
	j	endIf0
else0:
	lw	$t0, 4($fp)
	sw	$t0, -16($fp)
	lw	$t0, 4($fp)
	sw	$t0, -20($fp)
	addiu	$t0, $0, 1
	sw	$t0, -24($fp)
	lw	$t2, -20($fp)
	lw	$t3, -24($fp)
	sub	$t0, $t2, $t3
	sw	$t0, -28($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -28($fp)
	sw	$t0, 4($sp)
	jal	facF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -32($fp)
	lw	$t2, -16($fp)
	lw	$t3, -32($fp)
	mul	$t0, $t2, $t3
	sw	$t0, -36($fp)
	lw	$t0, -36($fp)
	sw	$t0, 8($fp)
	j	facFend
endIf0:
facFend:
	addi	$sp, $sp, 40
	jr	$ra
mainF:	move	$fp, $sp
	addi	$sp, $sp, -20
	la	$t0, aG
	sw	$t0, 0($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -12
	jal	getintF
	addi	$sp, $sp, 12
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -4($fp)
	lw	$t0, -4($fp)
	lw	$t1, 0($fp)
	sw	$t0, ($t1)
	la	$t0, aG
	sw	$t0, -8($fp)
	lw	$t0, -8($fp)
	lw	$t1, ($t0)
	sw	$t1, -8($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -8($fp)
	sw	$t0, 4($sp)
	jal	facF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -12($fp)
	sw	$fp, ($sp)
	sw	$ra, -4($sp)
	addi	$sp, $sp, -16
	lw	$t0, -12($fp)
	sw	$t0, 4($sp)
	jal	putintF
	addi	$sp, $sp, 16
	lw	$fp, ($sp)
	lw	$ra, -4($sp)
	lw	$t0, -8($sp)
	sw	$t0, -16($fp)
mainFend:
	addi	$sp, $sp, 20
	jr	$ra
putintF:
	li	$v0, 1
	lw	$a0, 4($sp)
	syscall
	jr	$ra
putstringF:
	li	$v0, 4
	lw	$a0, 4($sp)
	syscall
	jr	$ra
getintF:
	li	$v0, 5
	syscall
	sw	$v0, 4($sp)
	jr	$ra
getstringF:
	li	$v0, 8
	lw	$a0, 4($sp)
	li	$a1, 80
	syscall
	sw	$v0, 8($sp)
	jr	$ra
