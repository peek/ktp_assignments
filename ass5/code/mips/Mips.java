import java.util.*;
import java.io.*;

public class Mips {

    private static final String putintLabel = "putintF";
    private static final String putstringLabel = "putstringF";
    private static final String getintLabel = "getintF";
    private static final String getstringLabel = "getstringF";

    private PrintStream outStream;
    private boolean[] ioDef = {false,  // putint
                               false,  // putstring
                               false,  // getint
                               false}; // getstring

    public Mips(PrintStream _outStream, List<RtlDec> imCode)
        throws IOException
    {
        outStream = _outStream;
        generateCode(imCode);
    }

    public void generateCode(List<RtlDec> imCode)
    {
        //load data
        outputOp(".data");
        for (RtlDec d : imCode)
            if (d instanceof Data)
                generateData((Data)d);
        outputOp(".text");
        //start execution
        outputOp(".globl","main");
        outputLabelOp("main","sw","$ra","($sp)");
        outputOp("addi","$sp","$sp",-8);
        outputOp("jal","mainF");
        outputOp("addi","$sp","$sp",8);
        outputOp("lw","$ra","($sp)");
        outputOp("jr","$ra");
        for (RtlDec d : imCode)
            if (d instanceof Proc)
                generateProc((Proc)d);
        // generate "library" io fuctions
        generateIo();
    }

    public void generateData(Data data)
    {
        int size = data.getSize();
        int actSize = size % 4  == 0 ? size : ((size/4)+1)*4;
        outputLabelOp(data.getLabel(),".space", actSize);
    }

    public void generateProc(Proc proc)
    {
        switch (proc.getLabel()) {
        case putintLabel:
            ioDef[0] = true;
            break;
        case putstringLabel:
            ioDef[1] = true;
            break;
        case getintLabel:
            ioDef[2] = true;
            break;
        case getstringLabel:
            ioDef[3] = true;
            break;
        }
        outputLabelOp(proc.getLabel(),"move","$fp","$sp");
        int frameSize = proc.getFrame()
            +4*((proc.getLocals().size())
                -(proc.getFormals().size()));
        outputOp("addi","$sp","$sp",-frameSize);
        for (RtlInsn insn : proc.getInsns()) {
            if (insn instanceof Call) {
                generateCall((Call) insn,proc);
            } else if (insn instanceof Jump) {
                generateJump((Jump) insn,proc);
            } else if (insn instanceof CJump) {
                generateCJump((CJump) insn,proc);
            } else if (insn instanceof Eval) {
                generateEval((Eval) insn,proc);
            } else if (insn instanceof LabDef) {
                generateLabDef((LabDef) insn,proc);
            } else if (insn instanceof Load) {
                generateLoad((Load) insn,proc);
            } else if (insn instanceof Store) {
                generateStore((Store) insn,proc);
            } else {
                ;
            }
        }
        outputOp("addi","$sp","$sp",frameSize);
        outputOp("jr","$ra");
    }

    public String generateTempAccess(int temporary, Proc proc) {
        return generateTempAccess(temporary,proc,"$t0");
    }

    public String generateTempAccess(int temporary, Proc proc,
                                     String targetReg)
    {
        if (temporary == 1) {
            return "$fp";
        } else {
            int offset = calcOffset(temporary, proc);
            outputOp("lw",targetReg,offset+"($fp)");
            return targetReg;
        }
    }

    public void generateTempWrite(int temporary, Proc proc,
                                  String fromReg)
    {
        if (temporary == 1) {
            outputOp("move","$fp",fromReg);
        } else {
            int offset = calcOffset(temporary, proc);
            outputOp("sw", fromReg, offset + "($fp)");
        }
    }

    public Integer calcOffset(int temporary, Proc proc) {
        if (temporary == 1) {
            return null;
        } else {
            int formalscount = proc.getFormals().size();
            if (temporary == 0) {
                return (formalscount+1)*4;
            } else if (temporary < formalscount+2) {
                return (temporary-1)*4;
            } else {
                return -(proc.getFrame()+(temporary-formalscount-2)*4);
            }
        }
    }

    public void generateCall(Call callInsn, Proc proc)
    {
        List<Integer> tempArgList = callInsn.getArgs();
        outputOp("sw","$fp","($sp)");
        outputOp("sw","$ra","-4($sp)");
        outputOp("addi","$sp","$sp",-4*(3+tempArgList.size()));
        for (int i = 0; i < tempArgList.size(); i++) {
            int temp = tempArgList.get(i);
            generateTempAccess(temp,proc,"$t0");
            outputOp("sw","$t0",(4*(i+1))+"($sp)");
        }
        outputOp("jal",callInsn.getLabel());
        outputOp("addi","$sp","$sp",4*(3+tempArgList.size()));
        outputOp("lw","$fp","($sp)");
        outputOp("lw","$ra","-4($sp)");
        outputOp("lw","$t0","-8($sp)");
        generateTempWrite(callInsn.getTemp(),proc,"$t0");
    }

    public void generateJump(Jump jumpInsn, Proc proc)
    {
        outputOp("j", jumpInsn.getLabel());
    }

    public void generateCJump(CJump cjumpInsn, Proc proc)
    {
        String branchOp = cjumpInsn.getFlag() ? "bne" : "beq";
        String temp = generateTempAccess(cjumpInsn.getTemp(),proc);
        outputOp(branchOp, temp, "$0", cjumpInsn.getLabel());
    }

    public void generateEval(Eval evalInsn, Proc proc)
    {
        RtlExp expression = evalInsn.getExp();
        int temp = evalInsn.getTemp();
        if (expression instanceof Binary) {
            generateBinary((Binary) expression, proc);
            generateTempWrite(temp, proc, "$t0");
        } else if (expression instanceof Icon) {
            int wholeInt = ((Icon) expression).getVal();
            int upperInt = (int)(wholeInt & 0xFFFF0000L);
            int lowerInt = wholeInt & 0xFFFF;
            outputOp("addiu", "$t0", "$0", lowerInt);
            if (upperInt != 0) {
                outputOp("lui", "$t1", upperInt);
                outputOp("or", "$t0", "$t0", "$t1");
            }
            generateTempWrite(temp, proc, "$t0");
        } else if (expression instanceof TempExp) {
            String fromReg = generateTempAccess(((TempExp)expression).getTemp(),
                                                proc, "$t0");
            generateTempWrite(temp, proc, fromReg);
        } else if (expression instanceof LabRef) {
            outputOp("la","$t0", ((LabRef) expression).getLabel());
            generateTempWrite(temp, proc, "$t0");
        }
    }

    //Assume generateBinary stores result in $to
    public void generateBinary(Binary binExp, Proc proc)
    {
        String leftReg = generateTempAccess(binExp.getLeft(),proc,"$t2");
        String rightReg = generateTempAccess(binExp.getRight(),proc,"$t3");
        switch (binExp.getOp()) {
        case NE:
            outputOp("slt","$t0",leftReg,rightReg);
            outputOp("slt","$t1",rightReg,leftReg);
            outputOp("or","$t0","$t0","$t1");
            break;
        case EQ:
            outputOp("slt","$t0",leftReg,rightReg);
            outputOp("slt","$t1",rightReg,leftReg);
            outputOp("or","$t0","$t0","$t1");
            outputOp("xori","$t0","$t0",1);
            break;
        case LT:
            outputOp("slt","$t0",leftReg,rightReg);
            break;
        case GT:
            outputOp("slt","$t0",rightReg,leftReg);
            break;
        case LTEQ:
            outputOp("slt","$t0",rightReg,leftReg);
            outputOp("xori","$t0","$t0",1);
            break;
        case GTEQ:
            outputOp("slt","$t0",leftReg,rightReg);
            outputOp("xori","$t0","$t0",1);
            break;
        case PLUS:
            outputOp("add","$t0",leftReg,rightReg);
            break;
        case MINUS:
            outputOp("sub","$t0",leftReg,rightReg);
            break;
        case MUL:
            outputOp("mul","$t0",leftReg,rightReg);
            break;
        case DIV:
            outputOp("div",leftReg,rightReg);
            outputOp("mflo","$t0");
            break;
        }
    }

    public void generateLabDef(LabDef labdefInsn, Proc proc)
    {
        outputLabel(labdefInsn.getLabel());
    }

    public void generateLoad(Load loadInsn, Proc proc)
    {
        String loadOp = (loadInsn.getType() == RtlType.BYTE) ? "lb" : "lw";
        String address = generateTempAccess(loadInsn.getAddr(),proc,"$t0");
        outputOp(loadOp, "$t1", "(" + address + ")");
        generateTempWrite(loadInsn.getDest(),proc,"$t1");
    }

    public void generateStore(Store storeInsn, Proc proc)
    {
        String storeOp = (storeInsn.getType() == RtlType.BYTE) ? "sb" : "sw";
        String valueReg = generateTempAccess(storeInsn.getVal(),proc,"$t0");
        String addrReg = generateTempAccess(storeInsn.getAddr(),proc,"$t1");
        outputOp(storeOp, "$t0", "($t1)");
    }

    private void generateIo() {
        if (!ioDef[0]) { // putint
            outputLabel(putintLabel);
            outputOp("li", "$v0", 1);
            outputOp("lw", "$a0", 4+"($sp)");
            outputOp("syscall");
            outputOp("jr", "$ra");
        }
        if (!ioDef[1]) { // putstring
            outputLabel(putstringLabel);
            outputOp("li", "$v0", 4);
            outputOp("lw", "$a0", 4+"($sp)");
            outputOp("syscall");
            outputOp("jr", "$ra");
        }
        if (!ioDef[2]) { // getint
            outputLabel(getintLabel);
            outputOp("li", "$v0", 5);
            outputOp("syscall");
            outputOp("sw", "$v0", 4+"($sp)");
            outputOp("jr", "$ra");
        }
        if (!ioDef[3]) { // getstring
            outputLabel(getstringLabel);
            outputOp("li", "$v0", 8);
            outputOp("lw", "$a0", 4+"($sp)");
            outputOp("li", "$a1", 80); // The argument array must
                                       // have a length of at least 80.
            outputOp("syscall");
            outputOp("sw", "$v0", 8+"($sp)");
            outputOp("jr", "$ra");
        }
    }

    private void outputOp(String op, Object... args) {
        outStream.print("\t");
        outStream.print(op);
        if (args.length > 0) {
            outStream.print("\t");
            outStream.print(args[0]);
            for (int i = 1; i < args.length ; i++) {
                outStream.print(", ");
                outStream.print(args[i]);
            }
        }
        outStream.println();
    }

    private void outputLabel(String label) {
        outStream.print(label);
        outStream.println(":");
    }

    private void outputLabelOp(String label, String op, Object... args) {
        if (label.length() > 6) {
            outputLabel(label);
        } else {
            outStream.print(label);
            outStream.print(":");
        }
        outputOp(op, args);
    }
}