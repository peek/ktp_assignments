import java.util.*;

class GlobalEnv implements Env {
    Map<String, Type> global =  new HashMap<String, Type>();

    public void insert(String s, Type t, Position p)
        throws SemanticException
    {
        // System.out.println("Globally inserting "+s);
        if (global.get(s) != null) {
            Type prev = global.get(s);
            if (prev.getKind() == Kind.VAR ||
                prev.getKind() == Kind.ARR ||
                t.getKind() == Kind.VAR ||
                t.getKind() == Kind.ARR)
                SemanticCheck.
                  semanticError("Identifier '"+s+"' doubly defined", p);
            else if (Type.equivalentFuns(prev,t)) {
                if (prev.getKind() == Kind.EXT)
                    ;//donothing
                else if (t.getKind() == Kind.EXT)
                    return;
                else
                    SemanticCheck.
                        semanticError("Identifier '"+s+"' doubly defined", p);
            } else {
                SemanticCheck.
                    semanticError("Identifier '"+s+"' doubly defined", p);
            }
        }
        global.put(s,t);
    }

    public Type lookup(String s) {
        // System.out.println("Global lookup: "+s);
        return global.get(s);
    }

    public void setResult(Type t) {
        throw new IllegalStateException("No result type in global environment");
    }

    public Type getResult() {
        throw new IllegalStateException("No result type in global environment");
    }

    public Env enter() {
        return new LocalEnv(this);
    }
}
