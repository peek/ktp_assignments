import java.util.*;

enum ValType {INT, CHAR, VOID};
enum Kind {VAR, FUN, EXT, ARR};

class Type {
    private Kind kind;
    private ValType type;
    private List<Type> argList;

    public static Type fromNode(Node n)
        throws SemanticException {
        ValType createType = null;
        switch (n.getId()) {
        case CHAR:
                createType = ValType.CHAR;
                break;
        case INT:
            createType = ValType.INT;
            break;
        case VOID:
            createType = ValType.VOID;
                break;
        default:
            SemanticCheck.semanticError("Unknown type?",null);
        }
        return new Type(createType);
    }

    public Type (ValType t) {
        this(t,Kind.VAR,null);
    }

    public Type (ValType t,Kind k, List<Type> args) {
        kind = k;
        type = t;
        argList = args;
    }

    public ValType getType() {return type;}
    public Kind getKind() {return kind;}
    public void setKind(Kind k) {kind = k;}
    public List<Type> getArgTypes() {return argList;}

    //Specc: Returns TRUE if type1 is not more specific than type2
    public static boolean unify(Type type1, Type type2) {
        if ((type1.getKind()==Kind.ARR && type2.getKind()!=Kind.ARR) ||
            (type1.getKind()!=Kind.ARR && type2.getKind()==Kind.ARR))
            return false;
        ValType vt1 = type1.getType();
        ValType vt2 = type2.getType();
        if (type1.getKind()==Kind.ARR)
            return vt1==vt2;
        if (vt1==ValType.VOID || vt2==ValType.VOID)
            return vt1 == vt2;
        return true;
    }

    public boolean unifyWith(Type other) {
        return unify(this, other);
    }

    public static boolean equivalentFuns(Type fun1, Type fun2) {
        if (fun1.getType()!=fun2.getType())
            return false;
        List<Type> fun1args = fun1.argList;
        List<Type> fun2args = fun2.argList;
        if (fun1args.size() != fun2args.size())
            return false;
        Type f1a,f2a;
        for (int i = 0; i < fun1args.size(); i+=1) {
            f1a = fun1args.get(i);
            f2a = fun2args.get(i);
            if (f1a.getType()!=f2a.getType() ||
                f1a.getKind()!=f2a.getKind())
                return false;
        }
        return true;
    }

    public static Type fromBaseNode(Node n)
        throws SemanticException
    {
        switch (n.getId()) {
        case CHAR:
            return new Type(ValType.CHAR);
        case INT:
            return new Type(ValType.INT);
        case VOID:
            return new Type(ValType.VOID);
        default:
            SemanticCheck.
                semanticError("TypeNode is not of correct type!",
                              n.getPosition());
            return null;
        }
    }

    public String toString() {
        String retType = "";
        switch (type) {
        case CHAR:
            retType = "char";
            break;
        case INT:
            retType = "int";
            break;
        case VOID:
            retType = "void";
            break;
        }
        switch (kind) {
        case VAR:
            return retType;
        case ARR:
            return retType+"[]";
        default:
            if (argList.size() == 0) {return "(void) -> "+retType;}
            String prefix = ""+argList.get(0);
            for (int i = 1; i < argList.size(); i++) {
                prefix += " * "+argList.get(i);
            }
            return prefix + " -> " + retType;
        }
    }
}
