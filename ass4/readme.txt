The Makefile in the 'code' directory compiles the compiler.
The default target produces class files in the 'build' directory.
The 'jar' target produces a runnable jar file.
For further documentation of the Makefile see the Makefile.

The output for the requested testcases can be found in the 'tests' directory.
