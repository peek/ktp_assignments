import java.util.*;

class SemanticCheck {

    public static void attemptUnify(Type from, Type to, Position pos)
        throws SemanticException {
        if (!from.unifyWith(to)) {
            semanticError("Cannot unify " + from + " with " + to, pos);
        }
    }

    public static void semanticError(String s, Position p)
        throws SemanticException
    {
        String prefix;
        if (p == null) {
            prefix = "Semantic error:\n";
        } else {
            prefix = "Semantic error at " + p + ":\n";
        }
        throw new SemanticException(prefix+s);
    }

    public static boolean check (Node root)
        throws SemanticException
    {
        return checkProgram(root,new GlobalEnv());
    }

    public static boolean checkProgram(Node root, Env env)
        throws SemanticException
    {
        Node n;
        for (int i = 0; i < root.numberOfChildren(); i++) {
            n = root.getChild(i);
            switch (n.getId()) {
            case FUNC:
                checkFunction((FuncNode)n,env);
                break;
            case EXTERN:
                checkExtern((ExternNode)n,env);
                break;
            case SCALAR_DEC:
            case ARR_DEC:
                checkVarDec((VarDecNode)n,env);
                break;
            }
        }
        return true;
    }

    public static void checkFunction(FuncNode fun, Env env)
        throws SemanticException
    {
        Type returnType = Type.fromBaseNode(fun.getReturnType());
        List<Type> args = new ArrayList<Type>();
        Env local = env.enter();
        local.setResult(returnType);
        Type argType;
        for (Node n: fun.getFormals()) {
            argType = checkVarDec((VarDecNode)n,local);
            args.add(argType);
        }
        env.insert(((IdentifierNode)fun.getIdent()).getName(),
                   new Type(returnType.getType(),Kind.FUN,args),
                   fun.getPosition());
        checkCompoundStatement((CompoundStatementNode)fun.getBody(),local);
    }

    public static void checkExtern(ExternNode fun, Env env)
        throws SemanticException
    {
        Type returnType = Type.fromBaseNode(fun.getReturnType());
        List<Type> args = new ArrayList<Type>();
        Env local = env.enter();
        local.setResult(returnType);
        Type argType;
        for (Node n: fun.getFormals()) {
            argType = checkVarDec((VarDecNode)n,local);
            args.add(argType);
        }
        env.insert(((IdentifierNode)fun.getIdent()).getName(),
                   new Type(returnType.getType(),Kind.FUN,args),
                   fun.getPosition());
    }

    public static Type checkVarDec(VarDecNode varDec, Env env)
        throws SemanticException
    {
        Type type = Type.fromBaseNode(varDec.getChild(0));
        if (varDec.getId() == Id.ARR_DEC) {
            type.setKind(Kind.ARR);
        }
        String name = ((IdentifierNode)varDec.getChild(1)).getName();
        if (type.getType() == ValType.VOID)
            semanticError("Variable of type void",varDec.getPosition());
        env.insert(name,type,varDec.getPosition());
        return type;
    }

    public static void checkCompoundStatement(CompoundStatementNode node,
                                              Env env)
        throws SemanticException
    {
        checkDeclarations(node.getDeclarations(),env);
        checkStatements(node.getStatements(),env);
    }

    public static void checkDeclarations(List<Node> nodes, Env env)
        throws SemanticException
    {
        for (Node n : nodes) {
            checkVarDec((VarDecNode) n,env);
        }
    }

    //TODO: Implement checks for bodies of compound statements
    //If possible, make this same function be able to check
    //SimpleCompoundStatements
    public static void checkStatements(List<Node> nodes, Env env)
        throws SemanticException
    {
        for (Node n : nodes) {
            checkStatement(n,env);
        }
    }

    public static void checkStatement(Node node, Env env)
        throws SemanticException {
        switch (node.getId()) {
        case SIMPLE_COMPOUND_STMNT:
            checkStatements(((SimpleCompoundStatementNode) node).getStatements(), env);
            break;
        case EMPTY_STMNT:
            break;
        case IF:
            checkIf((IfNode) node, env);
            break;
        case WHILE:
            checkWhile((WhileNode) node, env);
            break;
        case RETURN:
            checkReturn((ReturnNode) node, env);
            break;
        default:
            checkExpression(node, env);
        }
    }

    public static Type checkExpression(Node node, Env env)
        throws SemanticException {
        switch (node.getId()) {
        case FCALL:
            return checkFCall((FCallNode) node, env);
        case BINARY:
            return checkBinOp((BinaryNode) node, env);
        case UNARY:
            return checkUnOp((UnaryNode) node, env);
        case ARRAY:
            return checkArrayGet((ArrayNode) node, env);
        case IDENT:
            return checkVariableGet((IdentifierNode) node, env);
        case INTEGER_LITERAL:
            return checkIntegerLit((IntegerLiteralNode) node, env);
        case CHARACTER_LITERAL:
            return checkCharLit((CharacterLiteralNode) node, env);
        case STRING_LITERAL:
            return checkStringLit((StringLiteralNode) node, env);
        default:
            semanticError("Unexpected node in expression:\n" + node,
                          node.getPosition());
            return null;
        }
    }

    //Note that all checks that check nodes in expressions should return
    //the type that the expression would return

    public static Type checkFCall(FCallNode node, Env env)
        throws SemanticException {
        String name = ((IdentifierNode) node.getIdent()).getName();
        Type sig = env.lookup(name);
        if (sig == null) {
            semanticError("Undeclared function '"+name+"'", node.getPosition());
        } else if (sig.getKind() == Kind.EXT) {
            semanticError("Undefined function '"+name+"'", node.getPosition());
        } else if (sig.getKind() != Kind.FUN) {
            semanticError("Not a function '"+name+"'", node.getPosition());
        }
        List<Type> argTypes = sig.getArgTypes();
        List<Node> callArgs = node.getArgs();
        if (argTypes.size() != callArgs.size()) {
            semanticError("Wrong number of arguments in function call to '"
                          +name+"'",
                          node.getPosition());
        }
        for (int i = 0; i < argTypes.size(); i++) {
            Type callArgType = checkExpression(callArgs.get(i), env);
            attemptUnify(callArgType, argTypes.get(i),
                        callArgs.get(i).getPosition());
        }
        return new Type(sig.getType());
    }

    public static Type checkBinOp(BinaryNode node, Env env)
        throws SemanticException {
        if (node.getOp() == Binop.ASS) {
            Node leftNode = node.getChild(0,2);
            Type varType = null;
            if (leftNode.getId() == Id.IDENT) {
                varType = checkVariableGet((IdentifierNode) leftNode, env);
                if (varType.getKind() != Kind.VAR) {
                    semanticError("'"+
                                  ((IdentifierNode) leftNode).getName()+
                                  "' is not an lval", node.getPosition());
                }
            } else if (leftNode.getId() == Id.ARRAY) {
                varType = checkArrayGet((ArrayNode) leftNode, env);
            } else {
                semanticError("Assigning to non-lval", node.getPosition());
            }
            Type expType = checkExpression(node.getChild(1,2),env);
            attemptUnify(expType, varType, node.getPosition());
            return varType;
        } else {
            Type leftType = checkExpression(node.getChild(0,2),env);
            Type rightType = checkExpression(node.getChild(1,2),env);
            attemptUnify(leftType, new Type(ValType.INT), node.getPosition());
            attemptUnify(rightType, new Type(ValType.INT), node.getPosition());
            return new Type(ValType.INT);
        }
    }

    public static Type checkUnOp(UnaryNode node, Env env)
        throws SemanticException {
        Type type = checkExpression(node.getChild(0,1),env);
        attemptUnify(type, new Type(ValType.INT), node.getPosition());
        return new Type(ValType.INT);
    }

    public static Type checkArrayGet(ArrayNode node, Env env)
        throws SemanticException {
        String name = ((IdentifierNode) node.getChild(0,2)).getName();
        Type arrType = env.lookup(name);
        if (arrType == null) {
            semanticError("Undeclared variable '"+name+"'", node.getPosition());
        } else if (arrType.getKind() != Kind.ARR) {
            semanticError("Indexing non-array '"+name+"'", node.getPosition());
        }
        Type indexType = checkExpression(node.getChild(1,2), env);
        attemptUnify(indexType, new Type(ValType.INT),
                     node.getChild(1,2).getPosition());
        return new Type(arrType.getType());
    }

    public static Type checkVariableGet(IdentifierNode node, Env env)
        throws SemanticException {
        String name = node.getName();
        Type type = env.lookup(name);
        if (type == null) {
            semanticError("Undeclared variable '"+name+"'", node.getPosition());
        } else if (type.getKind() == Kind.EXT ||
                   type.getKind() == Kind.FUN) {
            semanticError("'"+name+"' is not a variable",
                          node.getPosition());
        }
        return type;
    }

    public static void checkIf(IfNode node, Env env)
        throws SemanticException
    {
        boolean ifElse = node.numberOfChildren() == 3;
        Type condType = checkExpression(node.getChild(0), env);
        attemptUnify(condType,new Type(ValType.INT),
                     node.getChild(0).getPosition());
        checkStatement(node.getChild(1),env);
        if (ifElse)
            checkStatement(node.getChild(2),env);
    }

    public static void checkWhile(WhileNode node, Env env)
        throws SemanticException
    {
        Type condType = checkExpression(node.getChild(0,2), env);
        attemptUnify(condType,new Type(ValType.INT),
                     node.getChild(0,2).getPosition());
        checkStatement(node.getChild(1,2),env);
    }

    public static void checkReturn(ReturnNode node, Env env)
        throws SemanticException
    {
        Type exprType = new Type(ValType.VOID);
        if (node.getChild(0,1) != null)
            exprType = checkExpression(node.getChild(0,1), env);
        attemptUnify(exprType,env.getResult(),node.getPosition());
    }

    public static Type checkCharLit(CharacterLiteralNode node, Env env) {
        return new Type(ValType.CHAR);
    }

    public static Type checkStringLit(StringLiteralNode node, Env env) {
        return new Type(ValType.CHAR,Kind.ARR,null);
    }

    public static Type checkIntegerLit(IntegerLiteralNode node, Env env) {
        return new Type(ValType.INT);
    }
}
    //TODO: Implement any additional neccecary checkS