import java.util.*;

class Rtl {
    public static final int RV = 0;
    public static final int FP = 1;

    private static final String globltemp = "globltemp";

    private int labelCount = 0;
    private String funName;
    private int nextTemp;
    private Map<String,RtlType> globalScope = new HashMap<String,RtlType>();
    private Map<String,Boolean> globalArrayCheck = new HashMap<String,Boolean>();
    private Map<String,RtlType> localScope;
    private Map<String,Integer> bindings;

    public static int sizeOf(RtlType t) {
        switch(t) {
        case BYTE : return 1;
        case LONG : return 4;
        default :
            return -1;
        }
    }

    public static String regToString(int reg) {
        if (reg==RV) return "RV";
        if (reg==FP) return "FP";
        return "#"+reg;
    }

    public static List<RtlDec> generateCode(Node root) {
        Rtl temp = new Rtl();
        return temp.generate(root);
    }

    private List<RtlDec> generate(Node root) {
        List<RtlDec> declarations = new ArrayList<RtlDec>();
        Node n;
        for (int i = 0; i < root.numberOfChildren(); i++) {
            n = root.getChild(i);
            switch (n.getId()) {
            case FUNC:
                declarations.add(generateFunctionDec((FuncNode)n));
                break;
            case SCALAR_DEC:
                declarations.add(generateScalarDec((VarDecNode)n));
                break;
            case ARR_DEC:
                declarations.add(generateArrayDec((VarDecNode)n));
                break;
            }
        }
        declarations.add(new Data(globltemp,1));
        return declarations;
    }

    private RtlDec generateScalarDec(VarDecNode n) {
        int size;
        String name = ((IdentifierNode)n.getChild(1)).getName();
        if (n.getChild(0).getId() == Id.CHAR) {
            size = 1;
            globalScope.put(name,RtlType.BYTE);
        }
        else {
            size = 4;
            globalScope.put(name,RtlType.LONG);
        }
        globalArrayCheck.put(name,false);
        return new Data(name+"G",size);
    }

    private RtlDec generateArrayDec(VarDecNode n) {
        int size;
        String name = ((IdentifierNode)n.getChild(1)).getName();
        if (n.getChild(0).getId() == Id.CHAR) {
            size = 1;
            globalScope.put(name,RtlType.BYTE);
        }
        else {
            size = 4;
            globalScope.put(name,RtlType.LONG);
        }
        globalArrayCheck.put(name,true);
        size = size*(((IntegerLiteralNode)n.getChild(2)).getValue());
        return new Data(name+"G", size);
    }

    private RtlDec generateFunctionDec(FuncNode n) {
        nextTemp = 2;
        List<RtlInsn> body = new ArrayList<RtlInsn>();
        List<Integer> formals = new ArrayList<Integer>();
        List<Integer> locals = new ArrayList<Integer>();
        localScope = new HashMap<String,RtlType>();
        bindings = new HashMap<String,Integer>();
        String actName = ((IdentifierNode) n.getIdent()).getName();
        funName = actName+"F";
        body.add(new LabDef(funName));
        for (Node m : n.getFormals()) {
            formals.add(nextTemp);
            bindings.put(((IdentifierNode)m.getChild(1)).getName(),nextTemp);
            RtlType type;
            if (m.getChild(0).getId() == Id.CHAR)
                type = RtlType.BYTE;
            else
                type = RtlType.LONG;
            localScope.put(((IdentifierNode)m.getChild(1)).getName(),type);
            nextTemp++;
        }
        int size = 0;
        for (Node m : ((CompoundStatementNode)n.getBody()).getDeclarations()) {
            bindings.put(((IdentifierNode)m.getChild(1)).getName(),nextTemp);
            RtlType type;
            if (m.getChild(0).getId() == Id.CHAR)
                type = RtlType.BYTE;
            else
                type = RtlType.LONG;
            if (m.numberOfChildren() == 3) {
                body.add(new Eval(nextTemp,new Icon(size)));
                body.add(new Eval(nextTemp,
                                  new Binary(RtlBinop.PLUS,nextTemp,FP)));
                int arraySize = (type == RtlType.BYTE ? 1 : 4) *
                    ((IntegerLiteralNode)m.getChild(2)).getValue();
                size += arraySize % 4 == 0 ? arraySize : ((arraySize/4)+1)*4;
            }
            localScope.put(((IdentifierNode)m.getChild(1)).getName(),type);
            nextTemp++;
        }
        generateBody(((CompoundStatementNode)n.getBody()).getStatements(),body);
        for (int i = 2;i < nextTemp;i++) {
            locals.add(i);
        }
        body.add(new LabDef(funName+"end"));
        return new Proc(funName,
                        formals,
                        locals,
                        size,
                        body);
    }

    private void generateBody(List<Node> body, List<RtlInsn> previousCode)
    {
        for (Node n : body) {
            generateStatement(n,previousCode);
        }
    }

    private void generateStatement(Node n, List<RtlInsn> previousCode)
    {
        switch (n.getId()) {
        case SIMPLE_COMPOUND_STMNT:
            generateBody(((SimpleCompoundStatementNode)n).getStatements(),
                                previousCode);
            break;
        case IF:
            generateIf((IfNode) n, previousCode);
            break;
        case WHILE:
            generateWhile((WhileNode) n, previousCode);
            break;
        case RETURN:
            generateReturn((ReturnNode) n, previousCode);
            break;
        case EMPTY_STMNT:
            break;
        default:
            generateExpression(n, previousCode);
            break;
        }
    }

    private void generateIf(IfNode n, List<RtlInsn> previousCode)
    {
        int boolResultTemp = generateExpression(n.getChild(0),previousCode);
        String ifElseLabel = "else"+labelCount;
        String ifSkipLabel = "endIf"+labelCount;
        labelCount++;
        previousCode.add(new CJump(false,boolResultTemp,
                                   (n.numberOfChildren() == 3) ?
                                   ifElseLabel : ifSkipLabel));
        generateStatement(n.getChild(1),previousCode);
        if (n.numberOfChildren() == 3) {
            previousCode.add(new Jump(ifSkipLabel));
            previousCode.add(new LabDef(ifElseLabel));
            generateStatement(n.getChild(2),previousCode);
        }
        previousCode.add(new LabDef(ifSkipLabel));
    }

    private void generateWhile(WhileNode n, List<RtlInsn> previousCode)
    {
        String whileLoopLabel = "loop"+labelCount;
        String whileEndLabel = "endLoop"+labelCount;
        labelCount++;
        previousCode.add(new LabDef(whileLoopLabel));
        int boolResultTemp = generateExpression(n.getChild(0),previousCode);
        previousCode.add(new CJump(false,boolResultTemp,whileEndLabel));
        generateStatement(n.getChild(1),previousCode);
        previousCode.add(new Jump(whileLoopLabel));
        previousCode.add(new LabDef(whileEndLabel));
    }

    private void generateReturn(ReturnNode n, List<RtlInsn> previousCode)
    {
        if (n.getChild(0) != null) {
            int returnTemp = generateExpression(n.getChild(0), previousCode);
            previousCode.add(new Eval(RV,new TempExp(returnTemp)));
        }
        previousCode.add(new Jump(funName+"end"));
    }

    private int generateExpression(Node n, List<RtlInsn> previousCode)
    {
        switch (n.getId()) {
        case FCALL:
            return generateFCall((FCallNode) n, previousCode);
        case BINARY:
            return generateBinOp((BinaryNode) n, previousCode);
        case UNARY:
            return generateUnaryOp((UnaryNode) n, previousCode);
        case ARRAY:
            return generateArrayGet((ArrayNode) n, previousCode);
        case IDENT:
            return generateVariableGet((IdentifierNode) n, previousCode);
        case INTEGER_LITERAL:
            return generateIntegerLit((IntegerLiteralNode) n, previousCode);
        case CHARACTER_LITERAL:
            return generateCharLit((CharacterLiteralNode) n, previousCode);
            //case STRING_LITERAL:
            //return generateStringLit((StringLiteralNode) n, previousCode);
        }
        return nextTemp;
    }

    private int generateFCall(FCallNode node, List<RtlInsn> previousCode)
    {
        List<Integer> argTempList = new ArrayList<Integer>();
        int currArg;
        for (Node arg : node.getArgs()) {
            currArg = generateExpression(arg,previousCode);
            argTempList.add(currArg);
        }
        int returnTemp = nextTemp++;
        previousCode.add(new Call(returnTemp,
                                  ((IdentifierNode)node.getIdent()).getName(),
                                  argTempList));
        return returnTemp;
    }

    private static RtlBinop opToOp (Binop op) {
        switch (op) {
        case EQ:
            return RtlBinop.EQ;
        case NE:
            return RtlBinop.NE;
        case LT:
            return RtlBinop.LT;
        case GT:
            return RtlBinop.GT;
        case LTEQ:
            return RtlBinop.LTEQ;
        case GTEQ:
            return RtlBinop.GTEQ;
        case PLUS:
            return RtlBinop.PLUS;
        case MINUS:
            return RtlBinop.MINUS;
        case MUL:
            return RtlBinop.MUL;
        case DIV:
            return RtlBinop.DIV;
        default:
            return null;
        }
    }

    private int generateBinOp(BinaryNode node, List<RtlInsn> previousCode)
    {
        if (node.getOp() == Binop.ASS)
            return generateAssignment(node, previousCode);
        else {
            if (node.getOp() == Binop.OROR || node.getOp() == Binop.ANDAND)
                return generateLazyBinOp(node, previousCode);
            else {
                int arg1Temp = generateExpression(node.getChild(0),
                                                  previousCode);
                int arg2Temp = generateExpression(node.getChild(1),
                                                  previousCode);
                int returnTemp = nextTemp++;
                previousCode.add(new Eval(returnTemp,
                                          new Binary(Rtl.opToOp(node.getOp()),
                                                     arg2Temp,arg1Temp)));
                return returnTemp;
            }
        }
    }

    private int generateAssignment(BinaryNode node, List<RtlInsn> previousCode)
    {
        if (node.getChild(0).getId() == Id.ARRAY) {
            int tempAddress;
            String name = ((IdentifierNode)node.getChild(0).getChild(0)).getName();
            RtlType type;
            if (bindings.get(name)!= null) {
                tempAddress = bindings.get(name);
                type = localScope.get(name);
            }
            else {
                tempAddress = nextTemp++;
                previousCode.add(new Eval(tempAddress,new LabRef(name+"G")));
                type = globalScope.get(name);
            }
            int offsetTemp  = generateExpression(node.getChild(0).getChild(1),
                                          previousCode);
            if (type == RtlType.LONG) {
                int const4Temp = nextTemp++;
                previousCode.add(new Eval(const4Temp,new Icon(4)));
                previousCode.add(new Eval(offsetTemp,
                                 new Binary(RtlBinop.MUL,offsetTemp,
                                            const4Temp)));
            }
            int actAddress = nextTemp++;
            previousCode.add(new Eval(actAddress,
                             new Binary(RtlBinop.PLUS,offsetTemp,tempAddress)));
            int assignTemp = generateExpression(node.getChild(1),previousCode);
            previousCode.add(new Store(type,actAddress,assignTemp));
            //in case of char, return should be the casted value
            if (type == RtlType.BYTE)
                previousCode.add(new Load(type,actAddress,assignTemp));
            return assignTemp;
        } else {
            String name = ((IdentifierNode)node.getChild(0)).getName();
            if (bindings.get(name)==null) {
                int actAddress = nextTemp++;
                previousCode.add(new Eval(actAddress,new LabRef(name+"G")));
                int assignTemp = generateExpression(node.getChild(1),
                                                    previousCode);
                previousCode.add(new Store(globalScope.get(name),actAddress,
                                           assignTemp));
                //in case of char, return should be the casted value
                if (globalScope.get(name) == RtlType.BYTE)
                    previousCode.add(new Load(globalScope.get(name),actAddress,
                                              assignTemp));
                return assignTemp;
            } else {
                int assignTemp = generateExpression(node.getChild(1),
                                                    previousCode);
                //Code for casting to char
                if (localScope.get(name) == RtlType.BYTE) {
                    int globalTemp = nextTemp++;
                    previousCode.add(new Eval(globalTemp,
                                              new LabRef(globltemp)));
                    previousCode.add(new Store(RtlType.BYTE,
                                               globalTemp,assignTemp));
                    previousCode.add(new Load(RtlType.BYTE,
                                              globalTemp,assignTemp));
                }
                previousCode.add(new Eval(bindings.get(name),
                                          new TempExp(assignTemp)));
                return assignTemp;
            }
        }
    }

    private int generateLazyBinOp(BinaryNode node, List<RtlInsn> previousCode) {
        if (node.getOp() == Binop.OROR) {
            String trueLabel = "lazyTrue"+labelCount;
            String doneLabel = "lazyDone"+labelCount;
            labelCount++;
            int result1 = generateExpression(node.getChild(0), previousCode);
            previousCode.add(new CJump(true,result1,trueLabel));
            int result2 = generateExpression(node.getChild(1), previousCode);
            int resultTemp = nextTemp++;
            previousCode.add(new CJump(true,result2,trueLabel));
            previousCode.add(new Eval(resultTemp,new Icon(0)));
            previousCode.add(new Jump(doneLabel));
            previousCode.add(new LabDef(trueLabel));
            previousCode.add(new Eval(resultTemp,new Icon(1)));
            previousCode.add(new LabDef(doneLabel));
            return resultTemp;
        } else {
            String falseLabel = "lazyFalse"+labelCount;
            String doneLabel = "lazyDone"+labelCount;
            labelCount++;
            int result1 = generateExpression(node.getChild(0), previousCode);
            previousCode.add(new CJump(false,result1,falseLabel));
            int result2 = generateExpression(node.getChild(0), previousCode);
            int resultTemp = nextTemp++;
            previousCode.add(new CJump(false,result2,falseLabel));
            previousCode.add(new Eval(resultTemp,new Icon(1)));
            previousCode.add(new Jump(doneLabel));
            previousCode.add(new LabDef(falseLabel));
            previousCode.add(new Eval(resultTemp,new Icon(0)));
            previousCode.add(new LabDef(doneLabel));
            return resultTemp;
        }
    }

    private int generateUnaryOp(UnaryNode node, List<RtlInsn> previousCode) {
        int const0Temp = nextTemp++;
        previousCode.add(new Eval(const0Temp, new Icon(0)));
        int argTemp = generateExpression(node.getChild(0), previousCode);
        previousCode.add(new Eval(argTemp,
                                  new Binary(node.getOp() == Unop.NEG ?
                                             RtlBinop.MINUS : RtlBinop.EQ,
                                             argTemp, const0Temp)));
        return argTemp;
    }

    private int generateArrayGet(ArrayNode n, List<RtlInsn> previousCode) {
        String name = ((IdentifierNode) n.getChild(0)).getName();
        RtlType type;
        int baseAddressTemp;
        if (bindings.get(name) != null) {
            baseAddressTemp = bindings.get(name);
            type = localScope.get(name);
        } else {
            baseAddressTemp = nextTemp++;
            previousCode.add(new Eval(baseAddressTemp, new LabRef(name+"G")));
            type = globalScope.get(name);
        }
        int indexTemp = generateExpression(n.getChild(1), previousCode);
        if (type == RtlType.LONG) {
            int const4Temp = nextTemp++;
            previousCode.add(new Eval(const4Temp,new Icon(4)));
            previousCode.add(new Eval(indexTemp,
                                      new Binary(RtlBinop.MUL,
                                                 indexTemp,const4Temp)));
        }
        int actAddressTemp = nextTemp++;
        previousCode.add(new Eval(actAddressTemp,
                                  new Binary(RtlBinop.PLUS,
                                             baseAddressTemp, indexTemp)));
        int returnTemp = nextTemp++;
        previousCode.add(new Load(type, actAddressTemp, returnTemp));
        return returnTemp;
    }

    private int generateVariableGet(IdentifierNode n,
                                    List<RtlInsn> previousCode) {
        String name = n.getName();
        int returnTemp = nextTemp++;
        if (bindings.get(name) != null) {
            previousCode.add(new Eval(returnTemp,
                                      new TempExp(bindings.get(name))));
        } else {
            RtlType type = globalScope.get(name);
            previousCode.add(new Eval(returnTemp, new LabRef(name+"G")));
            if (!globalArrayCheck.get(name)) {
                previousCode.add(new Load(type,returnTemp,returnTemp));
            }
        }
        return returnTemp;
    }

    private int generateIntegerLit(IntegerLiteralNode n,
                                   List<RtlInsn> previousCode) {
        int returnTemp = nextTemp++;
        previousCode.add(new Eval(returnTemp, new Icon(n.getValue())));
        return returnTemp;
    }

    private int generateCharLit(CharacterLiteralNode n,
                                List<RtlInsn> previousCode) {
        int returnTemp = nextTemp++;
        previousCode.add(new Eval(returnTemp, new Icon((int)n.getValue())));
        return returnTemp;
    }
}