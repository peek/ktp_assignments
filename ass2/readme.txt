To run the compiler use the Makefile in the "code/" directory.

The default make target generates all the necessary files.
The main class will be "code/build/UcParse.class"

The target "make jar" also generates an executable jar-file "code/ucc.jar".

The program accepts a filename as a parameter and
prints results to standard out.


The "tests/" directory contain outputs for all requested test cases.
