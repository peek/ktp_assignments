PARSER_BEGIN(UcParse)

import java.util.*;
import java.io.*;

class UcParse {
    public static void main(String[] arg)
        throws IOException {
        if (arg.length != 1) {
            System.out.println("Usage: UcParse <input file name>");
            System.exit(0);
        }
        InputStream is = new FileInputStream(arg[0]);

        UcParse parser = new UcParse(is);
        try {
            Node tree = Start();
            tree.print("");
        } catch (TokenMgrError tme) {
            System.out.println(tme.getMessage());
        } catch (ParseException pe) {
            System.out.println(pe.getMessage());
        }
    }

    public static byte stringToByte(String from) throws ParseException {
        if (from.length() == 3)
            return (byte) from.charAt(1);
        else
            return eCharToByte(from.charAt(2));
    }

    public static byte eCharToByte(char from) throws ParseException  {
        switch (from) {
        case '\'':
            return (byte) 0x27;
        case 'n':
            return (byte) 0x0A;
        case '\"':
            return (byte) 0x22;
        case '?':
            return (byte) 0x3F;
        case 'r':
            return (byte) 0x0D;
        case 't':
            return (byte) 0x09;
        case 'v':
            return (byte) 0x0B;
        case 'a':
            return (byte) 0x07;
        case 'b':
            return (byte) 0x08;
        case '0':
            return (byte) 0x00;
        case 'f':
            return (byte) 0x0C;
        case '\\':
            return (byte) 0x5C;
        default:
            throw new ParseException("Unsupported escape sequence"+from);
        }
    }

    public static List<Node> stringToNodeList(String from)
        throws ParseException {
        System.out.println(from);
        List<Node> array = new ArrayList<Node>();
        for (int index = 1; index < from.length()-1; index += 1) {
            if (from.charAt(index) == '\\') {
                array.add(new CharacterLiteralNode(
                         eCharToByte(from.charAt(index+1)),
                null));
                index += 1;
            } else {
                array.add(new CharacterLiteralNode(
                         (byte) from.charAt(index),null));
            }
        }
        array.add(new CharacterLiteralNode((byte) 0x00,null));
        return array;
    }
}

PARSER_END(UcParse)


SKIP : {  " "
       | "\n"
       | "\r"
       | "\f"
       | "\t"
       | <"//" (~["\n","\r","\f"])*>
       | "/*": MULTI_COMMENT
       }

<MULTI_COMMENT> SKIP : {"*/" : DEFAULT}
<MULTI_COMMENT> MORE : {<~[]>}

TOKEN : {<CHAR   : "char">  }
TOKEN : {<ELSE   : "else">  }
TOKEN : {<IF     : "if" >   }
TOKEN : {<INT    : "int">   }
TOKEN : {<RETURN : "return">}
TOKEN : {<VOID   : "void">  }
TOKEN : {<WHILE  : "while"> }

TOKEN : {<NOTEQ  : "!=">}
TOKEN : {<NOT    : "!"> }
TOKEN : {<ANDAND : "&&">}
TOKEN : {<OROR   : "||">}
TOKEN : {<LPAREN : "("> }
TOKEN : {<RPAREN : ")"> }
TOKEN : {<MUL    : "*"> }
TOKEN : {<PLUS   : "+"> }
TOKEN : {<COMMA  : ","> }
TOKEN : {<MINUS  : "-"> }
TOKEN : {<DIV    : "/"> }
TOKEN : {<SEMI   : ";"> }
TOKEN : {<LTEQ   : "<=">}
TOKEN : {<LT     : "<"> }
TOKEN : {<EQEQ   : "==">}
TOKEN : {<EQ     : "="> }
TOKEN : {<GTEQ   : ">=">}
TOKEN : {<GT     : ">"> }
TOKEN : {<LBRACK : "["> }
TOKEN : {<RBRACK : "]"> }
TOKEN : {<LBRACE : "{"> }
TOKEN : {<RBRACE : "}"> }

TOKEN : {<#DIGIT : ["0"-"9"]>}
TOKEN : {<#ALPHA : ["a"-"z"]|["A"-"Z"]>}
TOKEN : {<#ECHAR : (~["'","\\"])|("\\"~[])>}

TOKEN : {<STRING : "\""(<ECHAR>|"'")*"\"">}

TOKEN : {<INTEGER_CONSTANT : (<DIGIT>)+>}
TOKEN : {<CHARACTER_CONSTANT : "'"(<ECHAR>)"'"> }

TOKEN : {<IDENT : ("_"|<ALPHA>)("_"|<ALPHA>|<DIGIT>)*>}


Node Start() :
{
    Node prog = new Node(Id.PROGRAM);
    Node td;
}
{
    (
        td = TopLevelDeclaration()
        {prog.add(td);}
    )*
    <EOF>
    {return prog;}
}

Node TopLevelDeclaration() :
{
    Node bt, dr, body;
    List<Node> formals;
}
{
    bt = BaseType()
    dr = Declarator()
    (
        formals = FunctionParameters()
        (
            body = CompoundStatement()
            {return new FuncNode(dr, bt, formals, body);}
        |
            <SEMI>
            {return new ExternNode(dr, bt, formals);}
        )
    |
        <SEMI>
        {return new VarDecNode(bt, dr);}
    )
}

List<Node> FunctionParameters () :
{
    List<Node> paramList = new ArrayList<Node>();
    Node f;
}
{
    <LPAREN>
    (
        <VOID>
    |
        f = Declaration()
        {paramList.add(f);}
        (
            <COMMA>
            f = Declaration()
            {paramList.add(f);}
        )*
    )
    <RPAREN>
    {return paramList;}
}

Node Declaration () :
{
    Node bt, dr;
}
{
    bt = BaseType()
    dr = Declarator()
    {return new VarDecNode(bt, dr);}
}

List<Node> AbstractFunctionParameters () :
{
    List<Node> l;
}
{
    l = FunctionParameters ()
    {return l;}
}

Node BaseType () :
{ }
{
    <CHAR>
    {return new Node (Id.CHAR);}
|
    <INT>
    {return new Node (Id.INT);}
|
    <VOID>
    {return new Node (Id.VOID);}
}

Node Declarator () :
{
    Node id, r = null;
    Token t;
}
{
    id = Identifier()
    (
        <LBRACK>
        (   r = IntegerLiteral()
        )?
        t = <RBRACK>
        {return new ArrDecNode(id, r, Position.fromToken(t));}
    |
        {return new ScalDecNode(id);}
    )
}

Node CompoundStatement () :
{
    List<Node> declarations = new ArrayList<Node>();
    List<Node> statements = new ArrayList<Node>();
    Node d, s;
}
{
    <LBRACE>
    (
        d = Declaration()
        {declarations.add(d);}
    <SEMI>
    )*
    (
        s = Statement()
        {statements.add(s);}
    )*
    <RBRACE>
    {return new CompoundStatementNode(declarations, statements);}
}

Node Statement() :
{
    Node c = null, s1, s2, r;
    Token t;
}
{
    s1 = SimpleCompoundStatement()
    {return s1;}
|
    s1 = Expression()
    <SEMI>
    {return s1;}
|
    <SEMI>
    {return new Node(Id.EMPTY_STMNT);}
|
    t = <IF>
    {s2 = null;}
    <LPAREN>
    c = Expression()
    <RPAREN>
    s1 = Statement()
    (
        LOOKAHEAD(1)
        <ELSE>
        s2 = Statement()
    )?
    {return new IfNode(c, s1, s2, Position.fromToken(t));}
|
    t = <WHILE>
    <LPAREN>
    c = Expression()
    <RPAREN>
    s1 = Statement()
    {return new WhileNode(c, s1, Position.fromToken(t));}
|
    t = <RETURN>
    (c = Expression())?
    <SEMI>
    {return new ReturnNode(c, Position.fromToken(t));}
}

Node SimpleCompoundStatement() :
{
    Node s;
    List<Node> statements;
    Token l, r;
}
{
    {statements = new ArrayList<Node>();}
    l = <LBRACE>
    (
        s = Statement()
        {statements.add(s);}
    )*
    r = <RBRACE>
    {
        s = new SimpleCompoundStatementNode(statements);
        s.setPosition(Position.combine(Position.fromToken(l),
                                       Position.fromToken(r)));
        return s;
    }
}

Node Expression() :
{
    Node t;
}
{
    t = P2()
    {return t;}
}

Node P2() :
{
    Node l, r;
}
{
    l = P4()
    (
        <EQ>
        r = P2()
        {return new BinaryNode(Binop.ASS, l, r);}
    |
        {return l;}
    )
}

Node P4() :
{
    Node l, r;
}
{
    l = P5()
    (
        <OROR>
        r = P5()
        {l = new BinaryNode(Binop.OROR, l, r);}
    )*
    {return l;}
}

Node P5() :
{
    Node l, r;
}
{
    l = P9()
    (
        <ANDAND>
        r = P9()
        {l = new BinaryNode(Binop.ANDAND, l, r);}
    )*
    {return l;}
}

Node P9() :
{
    Binop o;
    Node l, r;
}
{
    l = P10()
    (
        (
            <EQEQ>
            {o = Binop.EQ;}
        |
            <NOTEQ>
            {o = Binop.NE;}
        )
        r = P10()
        {l = new BinaryNode(o, l, r);}
    )*
    {return l;}
}

Node P10() :
{
    Binop o;
    Node l, r;
}
{
    l = P12()
    (
        (
            <GT>
            {o = Binop.GT;}
        |
            <LT>
            {o = Binop.LT;}
        |
            <GTEQ>
            {o = Binop.GTEQ;}
        |
            <LTEQ>
            {o = Binop.LTEQ;}
        )
        r = P12()
        {l = new BinaryNode(o, l, r);}
    )*
    {return l;}
}

Node P12():
{
    Binop o;
    Node l, r;
}
{
    l = P13()
    (
        (
            <PLUS>
            {o = Binop.PLUS;}
        |
            <MINUS>
            {o = Binop.MINUS;}
        )
        r = P13()
        {l = new BinaryNode(o, l, r);}
    )*
    {return l;}
}

Node P13() :
{
    Binop o;
    Node l, r;
}
{
    l = P14()
    (
        (
            <MUL>
            {o = Binop.MUL;}
        |
            <DIV>
            {o = Binop.DIV;}
        )
        r = P14()
        {
            l = new BinaryNode(o, l, r);
        }
    )*
    {return l;}
}

Node P14() :
{
    Unop o;
    Node t;
}
{
    (
        <NOT>
        {o = Unop.NOT;}
    |
        <MINUS>
        {o = Unop.NEG;}
    )
    t = P14()
    {return new UnaryNode(o, t);}
|
    t = BottomLevel()
    {return t;}
}

Node BottomLevel () :
{
    Node exp, id;
    List<Node> args;
    Token t;
}
{
    exp = IntegerLiteral()
    {return exp;}
|
    exp = CharacterLiteral()
    {return exp;}
|
    exp = StringLiteral()
    {return exp;}
|
    <LPAREN>
    exp = Expression()
    <RPAREN>
    {return exp;}
|
    id = Identifier()
    (
        <LBRACK>
        exp = Expression()
        t = <RBRACK>
        {return new ArrayNode(id, exp, Position.fromToken(t));}
    |
        <LPAREN>
        {args = new ArrayList<Node>();}
        (
            exp = Expression()
            {args.add(exp);}
            (
                <COMMA>
                exp = Expression()
                {args.add(exp);}
            )*
        |
            { }
        )
        <RPAREN>
        {return new FCallNode(id, args);}
    |
        {return id;}
    )
}

Node Identifier() :
{
    Token t;
}
{
    t = <IDENT>
    {return new IdentifierNode(t.image, Position.fromToken(t));}
}


Node IntegerLiteral() :
{
    Token t;
}
{
    t = <INTEGER_CONSTANT>
    {
         return new IntegerLiteralNode(Integer.parseInt(t.image),
                                       Position.fromToken(t));
    }
}

Node CharacterLiteral():
{
    Token t;
}
{
    t = <CHARACTER_CONSTANT>
    {
        return new CharacterLiteralNode(stringToByte(t.image),
                                        Position.fromToken(t));
    }
}

Node StringLiteral():
{
    Token t;
}
{
    t = <STRING>
    {
        return new StringLiteralNode(stringToNodeList(t.image),
                                     Position.fromToken(t));
    }
}
