The source code for the lexer is located in lexer-test.jj

To run the lexer use the executable jar-file LexerTest.jar
(which operates on standard in and standard out).

Alternatively use 'make' to generate the code from source
(requires JavaCC and a Java compiler) and run the main
class LexerTest (also operates on standard in and standard out).
